<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        DB::table('settings')->insert([
            [
                'key' => 'site_title',
                'value' => 'Utopian Global'
            ],
            [
                'key' => 'site_logo',
                'value' => '',
            ],
            [
                'key' => 'site_icon',
                'value' => '',
            ],
            [
                'key' => 'site_copyright',
                'value' => '&copy; '.date('Y').'Utopian Global'
            ],
            [
                'key' => 'coin_name',
                'value' => 'Utopian Global'
            ],
            [
                'key' => 'coin_symbol',
                'value' => 'UTG'
            ],
            [
                'key' => 'coin_price',
                'value' => 0.5
            ],
            [
                'key' => 'coin_address',
                'value' => ''
            ],
            [
                'key' => 'referral_bonus',
                'value' => 10
            ],
            [
                'key' => 'mail_id',
                'value' => ''
            ],
            [
                'key' => 'fb_url',
                'value' => ''
            ],
            [
                'key' => 'referral_content',
                'value' => ''
            ],
            [
                'key' => 'twt_url',
                'value' => ''
            ],
            [
                'key' => 'currency',
                'value' => "$"
            ]
       
        ]);
    }
}
