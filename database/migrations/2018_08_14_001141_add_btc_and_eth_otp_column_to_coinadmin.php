<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBtcAndEthOtpColumnToCoinadmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('coinadmins', function ($table) {
            $table->integer('btc_otp')->nullable()->after('picture');
            $table->integer('eth_otp')->nullable()->after('btc_otp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coinadmins', function ($table) {
            $table->dropColumn('btc_otp');
            $table->dropColumn('eth_otp');
        });
    }
}
