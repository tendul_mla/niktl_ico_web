<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\HomeController;
use App\TransactionHistory;

class CustomCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bitcoin:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bitcoin Confirmation Check';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    \Log::info("Cron Runs:");

        $history = TransactionHistory::where('status', 'pending')->where('payment_mode', 'BTC')->get();

        foreach ($history as $historys) {
	        $request = new \Illuminate\Http\Request();
	        $request->replace(['tx_id' => $historys->address]);
            \Log::info($request->tx_id);
            $result = (new HomeController)->history($request);

            if($result['confirmations'] >= 3) {
                $passbook = (new HomeController)->passbook($historys->id);
                $historys->status = 'success';
                $historys->save();
	            \Log::info("Cron Runs:".$historys->id);
            }
            
        }
    }
}
