<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use GuzzleHttp\Client;
use App\Http\Controllers\HomeController;

use Mail;
use App\Mail\Signupwelcomemail;
use App\Mail\Referralsignupwelcomemail;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        $country=Country::get();
        return view('auth.register',compact('country'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:20|regex:(^([a-zA-z ]+)(\d+)?$)',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country_id' => 'required',
            //'referral' => 'exists:users,name',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        $name = "adminaccount";
        $email=strtolower($data['email']);
        
        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
        ];
        $body = ["method" => "personal_newAccount", "params" => [$email], "id" => 1];

        $url ="http://localhost:8545";


        $res = $client->post($url, [
            'headers' => $headers,
            'body' => json_encode($body),
        ]);

        $eth_address = json_decode($res->getBody(),true);
        //dd($eth_address);
        $param = [$name];

        $body = ['params' => $param, 'method' => 'getnewaddress'];
        $btc_address = (new HomeController)->npmcurl($body);

        $referral_by=Null;

        if(isset($data['referral'])){
            $ref_user=User::where('email_token', $data['referral'])->first();
            $referral_by=$ref_user->id;
        }

        $userdata=[
            'name' => $data['name'],
            'email' => $email,
            'password' => bcrypt($data['password']),
            'country_id' => $data['country_id'],
            //'referral_by' => isset($data['referral'])? User::where('email_token', $data['referral'])->first()->id : NULL,
            'referral_by' => $referral_by,
            // 'eth_address' =>'eth_address',
            // 'btc_address' =>'btc_address',
            'eth_address' =>$eth_address['result'],
            'btc_address' =>$btc_address,
            'email_token' => base64_encode($email),
            'ip' => $ip,
        ];

        //dd($userdata);

        $userdata1=[
            'name' => $data['name'],
            'email' => $email,
            'password' => $data['password'],
            'email_token' => base64_encode($email),
            'ip' => $ip,
        ];

        // $querystr=http_build_query($userdata1);
        // $url="https://wallet.dembycoin.io/siteapi/guzzlesaveuser?".$querystr;

        // $ch = curl_init(); 
        // curl_setopt($ch, CURLOPT_URL, $url);       
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
        // $head = curl_exec($ch); 
        // curl_close($ch);

        //dd($head);
        /*$client = new Client();
        $response = $client->get($url);*/
        /*$headers = [
            'Content-Type' => 'application/json',
        ];*/
        //$data1=['form_params' => $userdata ];
        //$response = $client->post($url,$data1);
        //$response1 = json_decode($response->getBody(),true);

        //dd($response1);


        Mail::to($email)->send(new Signupwelcomemail($userdata1));

        if(isset($data['referral'])){
            
            Mail::to($ref_user->email)->send(new Referralsignupwelcomemail($userdata1));
        }
        
        return User::create($userdata);



    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);
        
        \Session::flash('flash_success','Account created successfully, verify your account by your welcome mail from your mail account...');
        return redirect('/login');
        /*
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
                        */
    }




    public function verify($token)
    {

        $user = User::where('email_token',$token)->first();
        $user->verified = 1;
        if($user->save()){

            $userdata1=[                
                'email' => $user->email,                
            ];

            // $querystr=http_build_query($userdata1);
            // $url="https://wallet.dembycoin.io/siteapi/guzzleverifyuser?".$querystr;

            // $ch = curl_init(); 
            // curl_setopt($ch, CURLOPT_URL, $url);       
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            // $head = curl_exec($ch); 
            // curl_close($ch);

           \Session::flash('flash_success','Your account has verified successfully...');
           return redirect('/login');

        }
    }
}
