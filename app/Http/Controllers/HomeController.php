<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use Setting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\User;
use App\Bonus;
use App\CoinType;
use App\Document;
use App\Passbook;
use App\Promocode;
use App\KycDocument;
use App\TransactionHistory;

use Mail;
use App\Mail\Secondstep;
use App\Mail\Thirdstep;
use App\Mail\Changepasswordalert;
use App\Mail\Differentipmail;
use App\Mail\Referraltransactiondetail;
use App\Mail\Adminalert;
use App\Mail\Admincoin;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['webemail']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->erc_address == Null){

            return view('ercaddress');
        }

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if(!Auth::user()->btc_address) {

            $name = "adminaccount";

            $param = [$name];

            $body = ['params' => $param, 'method' => 'getnewaddress'];
            $btc_address = $this->npmcurl($body);
            if($btc_address == 0) {

               Auth::user()->btc_address = Null;
               Auth::user()->save();

            } else {
                Auth::user()->btc_address = $btc_address;
                Auth::user()->save();
            }
        }
        if(!Auth::user()->eth_address) {
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $body = ["method" => "personal_newAccount", "params" => [Auth::user()->email], "id" => 1];

            $url ="http://localhost:8545";

            $res = $client->post($url, [
                'headers' => $headers,
                'body' => json_encode($body),
            ]);
            $eth_address = json_decode($res->getBody(),true);
            Auth::user()->eth_address = $eth_address['result'];
            Auth::user()->save();
        }
        if(!Auth::user()->dest_tag) {

            $dest_tag = mt_rand(100000000, 999999999);
            Auth::user()->dest_tag = $dest_tag;
            Auth::user()->save();
        }if(!Auth::user()->ip) {

            Auth::user()->ip = $ip;
            Auth::user()->save();
        }

        /*if(Auth::user()->ip){
            if(Auth::user()->ip!=$ip){
                $user=Auth::user()->email;
                //$user="bhoopathi@appoets.com";
                $country=$this->ip_info($ip, "Country");

                $request=array();
                $request['email']=$user;
                $request['country']=$country;
                $request['current_ip']=$ip;

                Mail::to($user)->send(new Differentipmail($request));
            }
        }*/

        $User = Auth::user()->status;
        $cointype = CoinType::where('status', '1')->get();
        $now = Carbon::now();
        $bonuses = Bonus::where('to','>=',$now)->get();
        if(Setting::get('kyc_approval')) {
            if($User) {
                return view('hometest',compact('cointype','bonuses','User'));
            } else {
                return redirect('/kyc');
            }
        } else {
            return view('hometest',compact('cointype','bonuses','User'));
        }
    }


    public function ercaddressupdate(Request $request){
        try{

            $client = new Client;
            $coindetails = $client->get('https://api.etherscan.io/api?module=account&action=balance&address='.$request->address);
            $coindetails = json_decode($coindetails->getBody(),true);
            //dd($coindetails);
            if($coindetails['status'] == 1){
                Auth::user()->erc_address = $request->address;
                Auth::user()->save();
                return back()->with('flash_success','Address updated Successfully !');
            }else{
                return back()->with('flash_error',$coindetails['result']);
            }
            
        }catch(Exception $e){
            return back()->with('flash_error','something went wrong !');
        }
    }

    public function createbtcaddress(){
        
        $users=User::get();
        
        foreach ($users as $value) {
           
        
            $name = "adminaccount";

            $param = [$name];

            $body = ['params' => $param, 'method' => 'getnewaddress'];
            $btc_address = $this->npmcurl($body);

            $user=User::findOrFail($value->id);
            if($btc_address == 0) {

               $user->btc_address = Null;
               $user->save();

            } else {
                $user->btc_address = $btc_address;
                $user->save();
            }
        }
        return "Done";
        
    }

    public function createethaddress(){

        $users=User::get();
        
        foreach ($users as $value) {    

            $user=User::findOrFail($value->id);
            
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $body = ["method" => "personal_newAccount", "params" => [$user->email], "id" => 1];

            $url ="http://localhost:8545";

            $res = $client->post($url, [
                'headers' => $headers,
                'body' => json_encode($body),
            ]);
            $eth_address = json_decode($res->getBody(),true);


            $user->eth_address = $eth_address['result'];
            $user->save();
        }    

        return "Done";
        
    }

    public function indextest()
    {

        if(Auth::user()->erc_address == Null){

            return view('ercaddress');
        }

        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if(!Auth::user()->btc_address) {

            $name = "adminaccount";

            $param = [$name];

            $body = ['params' => $param, 'method' => 'getnewaddress'];
            $btc_address = $this->npmcurl($body);
            if($btc_address == 0) {

               Auth::user()->btc_address = Null;
               Auth::user()->save();

            } else {
                Auth::user()->btc_address = $btc_address;
                Auth::user()->save();
            }
        }
        if(!Auth::user()->eth_address) {
            $client = new Client();
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $body = ["method" => "personal_newAccount", "params" => [Auth::user()->email], "id" => 1];

            $url ="http://localhost:8545";

            $res = $client->post($url, [
                'headers' => $headers,
                'body' => json_encode($body),
            ]);
            $eth_address = json_decode($res->getBody(),true);
            Auth::user()->eth_address = $eth_address['result'];
            Auth::user()->save();
        }
        if(!Auth::user()->dest_tag) {

            $dest_tag = mt_rand(100000000, 999999999);
            Auth::user()->dest_tag = $dest_tag;
            Auth::user()->save();
        }if(!Auth::user()->ip) {

            Auth::user()->ip = $ip;
            Auth::user()->save();
        }

        /*if(Auth::user()->ip){
            if(Auth::user()->ip!=$ip){
                $user=Auth::user()->email;
                //$user="bhoopathi@appoets.com";
                $country=$this->ip_info($ip, "Country");

                $request=array();
                $request['email']=$user;
                $request['country']=$country;
                $request['current_ip']=$ip;

                Mail::to($user)->send(new Differentipmail($request));
            }
        }*/

        $User = Auth::user()->status;
        $cointype = CoinType::where('status', '1')->get();
        $now = Carbon::now();
        $bonuses = Bonus::where('to','>=',$now)->get();
        if(Setting::get('kyc_approval')) {
            if($User) {
                return view('home',compact('cointype','bonuses','User'));
            } else {
                return redirect('/kyc');
            }
        } else {
            return view('hometest',compact('cointype','bonuses','User'));
        }
    }

    public function profile(){

        return view('profile');
    }

    public function update_password(Request $request)
    {
        $this->validate($request, [
                'password' => 'required|min:6',
                'old_password' => 'required',
            ]);

        $User = Auth::user();

        if(Hash::check($request->old_password, $User->password))
        {
            if(Hash::check($request->password, $User->password))
            {
                return back()->with('flash_error', trans('sessionflash.new_old_passord_not_be_same'));
            } else {
                if($request->password==$request->password_confirmation){
                    $User->password = bcrypt($request->password);
                    $User->save();

                    $user_email=Auth::user()->email; 
                    Mail::to($user_email)->send(new Changepasswordalert($user_email));

                    //return back()->with('flash_success', trans('sessionflash.password_updated'));

                    Auth::logout();
                    \Session::flash('flash_success',trans('sessionflash.password_updated'));
                    return redirect('/login');
                    
                }else{

                    return back()->with('flash_error', trans('sessionflash.new_confirm_password_same'));
                }
            }
        } else {
            return back()->with('flash_error', trans('sessionflash.old_password_wrong'));
        }
    }

    public function profile_store(Request $request){
        $this->validate($request, [
            'name' => 'max:255',
            'email' => 'email|unique:users,email,'.Auth::user()->id,
        ]);

        try{
            $user = Auth::user();
            if($request->has('name')){
                $user->name = $request->name;
            }
            if($request->has('email')){
                $user->email = $request->email;
            }
            if($request->has('mobile')){
                $user->mobile = $request->mobile;
            }
            if($request->has('coin_address')){
                $user->coin_address = $request->coin_address;
            }
            $user->save();
            return back()->with('flash_success',trans('sessionflash.updated_successfully'));
        }catch(Exception $e){
            return back()->with('flash_error',$e->getMessage());
        }

    }

    public function achieve($id)
    {
        $Trans = TransactionHistory::where('id',$id)->first();
        $Trans->achieve = '1';
        $Trans->save();

        return back()->with('flash_success','Successfully achieve');
    }

    public function Address(Request $request){
        $this->validate($request, [
            'wallet_address' => 'required',
        ]);

        try{
            $client = new Client();
            $requestdata = $client->get('https://api.etherscan.io/api?module=account&action=balance&address='.$request->wallet_address.'&tag=latest');
            $response = json_decode($requestdata->getBody(),1);
            if($response['status'] == '0') {
                return back()->with('flash_error',trans('sessionflash.invalid_address'));
            }
            $user = Auth::user();
            if($request->has('wallet_address')){
                $user->coin_address = $request->wallet_address;
            }
            $user->save();
            return back()->with('flash_success',trans('sessionflash.updated_successfully'));
        }catch(Exception $e){
            return back()->with('flash_error',$e->getMessage());
        }

    }

    public function kyc()
    {
        try{

            $KycDocument = KycDocument::where('user_id',Auth::user()->id)->get();
            $Kyc = Document::orderBy('order','asc')->get();
            return view('kyc',compact('Kyc','KycDocument'));

        }catch(Exception $e){
            return back()->with('flash_error',$e->getMessage());
        }

    }

    public function kycProcess(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg',
        ]);

        try{
            foreach ($request->image as $key => $image) {
                $Document = KycDocument::where('user_id', Auth::user()->id)
                            ->where('document_id', $key)
                            ->delete();

                KycDocument::create([
                    'url' => $image->store('kyc/documents'),
                    'user_id' => Auth::user()->id,
                    'document_id' => $key,
                    'status' => 'PENDING',
                ]);
            }

            $KycCount = KycDocument::where('user_id',Auth::user()->id)->count();
            $DocCount = Document::count();

            if($KycCount == $DocCount){

                $first_name = Auth::user()->first_name;
                
                $email = Auth::user()->email;
                $maildata=[ 'first_name'=>$first_name,'email'=>$email];

                //dd($maildata);
                $adminemail = Setting::get('mail_id');
                Mail::to($adminemail)->send(new Adminalert($maildata));
            }
            return back()->with('flash_success',trans('sessionflash.updated_successfully'));
        }catch(Exception $e){
            return back()->with('flash_error',$e->getMessage());
        }

    }

    public function referral(Request $request)
    {
        try{

            $User = User::where('referral_by', Auth::user()->id)->get();

            $Transactions = User::where('referral_by', Auth::user()->id)->select('id')->get()->toArray();
            $val = [];

           foreach($Transactions  as $key=>$Trans)
           {
                array_push($val, $Trans['id']);
            }

            // dd($val);


            $Transactions = TransactionHistory::whereIn('user_id',$val)->get();
         //dd($Transactions);

            //$Transactions = TransactionHistory::with('user')->where('user_id',$User->id)->get();

            return view('referral',compact('User','Transactions'));
        }catch(Exception $e){
            return back()->with('flash_error',$e->getMessage());
        }

    }

    public function coinStatus(Request $request){

        try{
            $client = new Client();
            if($request->type == 'XRP'){
                $requestdata = $client->get('https://www.bitstamp.net/api/v2/ticker/xrpusd');
                //dd($requestdata);
            }
            if($request->type == 'ETH'){
                $requestdata = $client->get('https://api.etherscan.io/api?module=stats&action=ethprice&api');
            }
            if($request->type == 'BTC'){
                $requestdata = $client->get('https://www.bitstamp.net/api/v2/ticker/btcusd');
            }
            if($request->type == 'EUR'){
                $coinvalue = Setting::get('wire_euro_value');
                $response =  response()->json(['message' => 'OK', 'last' => $coinvalue], 200);
                
            }
            if($request->type == 'USD'){
                $coinvalue = Setting::get('wire_usd_value');
                $response =  response()->json(['message' => 'OK', 'last' => $coinvalue], 200);
                
            }

          if($request->type != 'EUR' && $request->type != 'USD'){
            $response = json_decode($requestdata->getBody(),1);
            $response['message'] = 'OK';
        }

            if($request->promo != null) {
                $this->check_expiry($request->promo);
                $promo_code = Promocode::where('promo_code', $request->promo)->first();
                $history = TransactionHistory::where('user_id', Auth::user()->id)->where('promocode', $request->promo)->first();
                if($history) {
                  return response()->json(['message' => 'error', 'error' => 'promo_used'], 200);
                }
                if($promo_code) {
                    if($promo_code->status == 'EXPIRED') {
                        return response()->json(['message' => 'error', 'error' => 'promo_expired'], 200);
                    } else {
                        $response['promo_percent'] = $promo_code->percentage;
                    }
                } else {
                    return response()->json(['message' => 'error', 'error' => 'invalid_promo'], 200);
                }
            }
            return $response;
        }catch(Exception $e){
            return response()->json(['message' => 'error', 'error' => 'id_not_valid'], 200);
        }
    }

    public function paymentsecondstep(Request $request){

        $user=Auth::user()->email;
        //$user="bhoopathi@appoets.com";
        
        Mail::to($user)->send(new Secondstep($request));
    }  

    public function paymentthirdstep($request){

        $user=Auth::user()->email;
        $first_name = Auth::user()->first_name;
        $adminemail = Setting::get('mail_id');


        $maildata=[ 'first_name'=>$first_name,'email'=>$user ,'invoice' => $request->transcation_invoice,'ico' => $request->ico,'amount'=> $request->amount_ctc];
        //$user="bhoopathi@appoets.com";
        
        Mail::to($user)->send(new Thirdstep($request));

        Mail::to($adminemail)->send(new Admincoin($maildata));
    }  

    public function check_expiry($promocode){
        try {
            $Promocode = Promocode::where('promo_code', $promocode)->get();
            foreach ($Promocode as $index => $promo) {
                if(date("Y-m-d") > $promo->expiration){
                    $promo->status = 'EXPIRED';
                    $promo->save();
                }
            }
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }
    }

     public function checkTrans(Request $request) {

        $tx = TransactionHistory::where('payment_id', $request->tx_id)->first();
        if($tx) {
            return 1;
        } else {
            return 0;
        }
    }


    public function checkTransaction(Request $request) {
        $this->validate($request, [
            'tranx_id' => 'required|unique:transaction_histories,payment_id',
            'amount' => 'required',
            'cointype' => 'required'
            ],[
            'tranx_id.unique' => 'The Transaction Id has already been used.',
        ]);

        $transaction_id = $request->tranx_id;

        $amount = $request->amount;

        try {
            $Coin = CoinType::where('symbol',$request->cointype)->first();
            if($request->cointype == 'ETH'){
                // $ether_id = $Coin->address;
                // $client = new \GuzzleHttp\Client();
                // $request = $client->get('https://api.blockcypher.com/v1/eth/main/txs/'.$transaction_id.'');
                // $response = json_decode($request->getBody());

                // if(strcasecmp($response->outputs[0]->addresses[0], substr($ether_id, 2)) == 0) {
                //     if(($response->total/1000000000000000000 - $amount) > -0.005) {
                        return response()->json(['success' => 'Ok'], 200);
                //     }else{
                //         return response()->json(['error' => 'price_not_match'], 200);
                //     }
                // }else{
                //     return response()->json(['error' => 'address_not_match'], 200);
                // }
            }
            if($request->cointype=='BTC'){
                // $bitcoin_id = $Coin->address;
                // $client = new \GuzzleHttp\Client();
                // $request = $client->get('https://blockexplorer.com/api/addr/'.$transaction_id.'');
                // $response = json_decode($request->getBody());

                // if(strcasecmp($response->outputs[0]->addresses[0], $bitcoin_id) == 0) {
                    // if(($response->outputs[0]->value/100000000 - $amount) > -0.0005) {
                        return response()->json(['success' => 'Ok'], 200);
                    // }else{
                    //     return response()->json(['error' => 'price_not_match'], 200);
                    // }
                // }else{
                //     return response()->json(['error' => 'address_not_match'], 200);
                // }
            }
            if($request->cointype=='EUR'){
                 return response()->json(['success' => 'Ok'], 200);
            }
            if($request->cointype=='XRP'){
                $ripple_id = $Coin->address;
                $client = new \GuzzleHttp\Client();
                $request = $client->get('https://data.ripple.com/v2/transactions/'.$transaction_id.'?binary=false');
                $response = json_decode($request->getBody());

                if($response->result == 'success') {
                    if($response->transaction->tx->Destination==$ripple_id ){
                        if(($response->transaction->tx->Amount/1000000 - $amount) > -0.5){
                            return response()->json(['success' => 'Ok'], 200);
                        }else{
                            return response()->json(['error' => 'price_not_match'], 200);
                        }
                    }else{
                        return response()->json(['error' => 'Failed'], 200);
                    }
                } else {
                    return response()->json(['error' => 'Failed'], 200);
                }
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return response()->json(['error' => 'id_not_valid'], 200);
        } catch(Exception $e){
            return response()->json(['error' => 'id_not_valid'], 200);
        }
    }

    public function Transaction(Request $request)
    {
        $this->validat($request,[
            'tranx_id' => 'required'
        ]);
        dd($request->all());
        try{
            $initial_ctc = $request->amount_ctc;
            $user = Auth::user();
            if($user->coin_address != $request->coin_address) {
                $user->coin_address = $request->coin_address;
            }
            if($request->cointype=='XRP'){
                $payment_mode = $request->cointype;
                $user->XRP += $request->amount_new;
                $status = 'success';

                $client = new Client;
                $xrpusd = $client->get('https://www.bitstamp.net/api/v2/ticker/xrpusd');
                $xrp_price = json_decode($xrpusd->getBody(),true);
                $xrp_price = $xrp_price['last'];

                $request->amount_ctc = round(($request->amount_new / Setting::get('coin_price')) * $xrp_price);

                 $dest_tag = mt_rand(100000000, 999999999);
                Auth::user()->update(['dest_tag' => $dest_tag]);
            }
            if($request->cointype=='BTC'){
                $payment_mode = $request->cointype;
                $user->BTC += $request->amount_new;
                $status = 'pending';

                $client = new Client;
                $bitstamp = $client->get('https://www.bitstamp.net/api/v2/ticker/btcusd/');
                $btc_price = json_decode($bitstamp->getBody(),true);
                $btc_price = $btc_price['last'];

                $request->amount_ctc = round(($request->amount_new / Setting::get('coin_price')) * $btc_price);

                $param = ["adminaccount"];

                $body = ['params' => $param, 'method' => 'getnewaddress'];
                $btc_address = $this->npmcurl($body);
                Auth::user()->update(['btc_address' => $btc_address]);
            }
            if($request->cointype=='ETH'){
                $payment_mode = $request->cointype;
                $user->ETH += $request->amount_new;
                $status = 'success';

                $client = new Client;
                $etherscan = $client->get('https://api.etherscan.io/api?module=stats&action=ethprice');
                $eth_price = json_decode($etherscan->getBody(),true);
                $eth_price = $eth_price['result']['ethusd'];

                $request->amount_ctc = round(($request->amount_new / Setting::get('coin_price')) * $eth_price);

                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                ];
                $body = ["method" => "personal_newAccount", "params" => [Auth::user()->email], "id" => 1];
                
                $url ="http://localhost:8545";
                $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);
                $eth_address = json_decode($res->getBody(),true);
                Auth::user()->update(['eth_address' => $eth_address['result']]);
            }
            if($request->cointype=='EUR'){
                $payment_mode = $request->cointype;
                $status = 'pending';
            }
            if($request->cointype=='USD'){
                $payment_mode = $request->cointype;
                $status = 'pending';
            }

            $request->priceusd = $request->amount_ctc * Setting::get('coin_price');

            $Transaction = new TransactionHistory;
            $Transaction->user_id = Auth::user()->id;
            $Transaction->price = $request->amount_new;
            $Transaction->payment_mode = $payment_mode;
            $Transaction->address = $request->address;
            $Transaction->bonus_point = $request->bonus_point;
            $Transaction->referal_code = $request->referal_code;
            $Transaction->promocode = $request->promo_code;
            $Transaction->payment_id = $request->tranx_id;
            $Transaction->usd = $request->priceusd;
            $Transaction->status = $status;
            $Transaction->ico_initial = $initial_ctc;
            $Transaction->ico = $request->amount_ctc;
            $Transaction->ico_price = Setting::get('coin_price');

            if(Auth::user()->referral_by)
            {
                $Transaction->referral_percentage = Setting::get('referral_bonus');
                $Transaction->referral_by = Auth::user()->referral_by;
                $Transaction->referral_amount = $request->amount_ctc*(Setting::get('referral_bonus')/100);

            }
            
            $Transaction->save();
           
            $user->save();

            if($request->cointype!='BTC' || $request->cointype!='EUR' || $request->cointype!='USD'){
                $passbook = $this->passbook($Transaction->id);
            }

            if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            $country=$this->ip_info($ip, "Country");   
            // -------------------- Payment Successful Mail -----------------    
            $maildata=[
                'usd'=>$request->priceusd,
                'price'=>$request->amount_new,
                'cointype'=>$request->cointype,
                'amount_ctc'=>$request->amount_ctc,
                'ico'=>Setting::get('coin_symbol'),
                'transcation_invoice'=>Setting::get('coin_symbol')."543".str_pad($Transaction->id, 5, '0', STR_PAD_LEFT),
                'country'=>$country,
            ];            
            $this->paymentthirdstep($maildata);
            // -------------------- Payment Successful Mail -----------------   


            // -------------------- Wallet amount update -----------------   
            /*$userwalletdata=[
                'email' => Auth::user()->email,
                'amount' => $request->amount_ctc,                
            ];
            $querystr=http_build_query($userwalletdata);
            $url="https://wallet.dembycoin.io/siteapi/guzzlewalletupdate?".$querystr;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);       
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            $head = curl_exec($ch); 
            curl_close($ch);*/
            // -------------------- Wallet amount update -----------------   


            // -------------------- Refferal Payment Successful Mail -----------------    
            if(Auth::user()->referral_by){
                $user=User::findOrFail(Auth::user()->referral_by);
                $ref_email=$user['email'];
                $maildata1=[
                    'user_name'=>Auth::user()->name,
                    'user_email'=>Auth::user()->email,
                    'usd'=>$request->priceusd,
                    'transcation_invoice'=>Setting::get('coin_symbol').str_pad($Transaction->id, 5, '0', STR_PAD_LEFT),
                    'price'=>$request->amount_new,
                    'cointype'=>$request->cointype,
                    'amount_ctc'=>$request->amount_ctc,
                    'ico'=>Setting::get('coin_symbol'),
                    'commission_percentage'=>Setting::get('referral_bonus'),
                    'commission_amount'=>$Transaction->referral_amount,
                ];            
                
                Mail::to($ref_email)->send(new Referraltransactiondetail($maildata1));
            }
            // -------------------- Refferal Payment Successful Mail -----------------   
               
            return back()->with('flash_success',trans('sessionflash.transaction_successfully_status_on'));
        } catch(Exception $e) {
            return back()->with('flash_error',$e->getMessage());
        }
    }

    public function passbook($id){
            $transaction = TransactionHistory::find($id);
            $user = User::find($transaction->user_id);

            $passbook = new Passbook;
            $passbook->user_id = $transaction->user_id;
            $passbook->ico = $transaction->ico;
            $passbook->via = 'PURCHASE';
            $passbook->save();
            $discount = $user->ico_bonus;
            if($transaction->bonus_point>0){
                $discount_bnc = $transaction->ico*($transaction->bonus_point/100);
                $discount += $discount_bnc;
                $passbook = new Passbook;
                $passbook->user_id = $transaction->user_id;
                $passbook->ico = $discount_bnc;
                $passbook->via = 'BONUS';
                $passbook->save(); 
            }
            if($user->referral_by != ''){
                $discount_ref = $transaction->ico*(Setting::get('referral_bonus')/100);
                // $discount += $discount_ref;
                $users = User::find($user->referral_by);
                $users->ico_bonus += $discount_ref;
                $users->save();

                $passbook = new Passbook;
                $passbook->user_id = $user->referral_by;
                $passbook->ico = $discount_ref;
                $passbook->via = 'REFERRAL';
                $passbook->save(); 
            }
            if($transaction->promocode!=''){
                $promo = Promocode::where('promo_code', $transaction->promocode)->first();
                $discount_promo = $transaction->ico*($promo->percentage/100);
                $discount += $discount_promo;
                $passbook = new Passbook;
                $passbook->user_id = $transaction->user_id;
                $passbook->ico = $discount_promo;
                $passbook->via = 'PROMOCODE';
                $passbook->save(); 
            }

            
            $user->ico_balance += $transaction->ico;
            $user->ico_bonus = $discount;
            $user->save();

            return response()->json(['success' => 'Ok'], 200);

    }

    public function getTransaction(Request $request)
    {
        $balance = Passbook::where('user_id', Auth::user()->id)->where('via', 'REFERRAL')->sum('ico');
        $Transactions = TransactionHistory::with('user')->where('user_id',Auth::user()->id)->where('achieve','0')->orderBy('id','desc')->get();
        return view('transactions',['Transactions' => $Transactions, 'referral_balance' => $balance]);
    }

	public function webemail(Request $request)
	{
		$data = $request->all();
		Mail::send('emails.website',$data, function ($m) {
        		$m->from('hello@app.com', 'Your Application');

	        	$m->to(Setting::get('mail_id'), "test")->subject('Your Reminder!');
        	});
	}

    public function history(Request $request)
    {
        //$tx = TransactionHistory::where('payment_id', $request->tx_id)->first();
        // if($tx) {
        //     return 1;
        // } else {
        //     return 0;
        // }
        $name = "adminaccount";
        $param=[$request->tx_id];
        $body = [                  
            'params' => $param,
            'method' => 'getreceivedbyaddress',        
        ];
        $curldata=$this->npmcurl($body);

        $details = $curldata;

        $param=[$name];
        $body = [
            'params' => $param,
            'method' => 'listtransactions',        
        ];
        $curldata=$this->npmcurl($body);

        $details1 = $curldata;
      

        foreach($details1 as $det )
        {

            if($det['address'] == $request->tx_id )
            { 

                if($det['confirmations'] >=0 )
                {
                    return $det;
                }

            }    
        }
        //dd($details1);

        return  $details;
          
    }

    public function npmcurl($body){
        
        try{
            $id=0;
            $status       = null;
            $error        = null;
            $raw_response = null;
            $response     = null;
            
            // $proto=env('BTC_PROTO');
            // $username =env('BTC_USERNAME');
            // $password =env('BTC_PASSWORD');
            // $host =env('BTC_HOST');
            // $port =env('BTC_PORT');
            $proto="http";
            $username ="admin";
            $password ="123456";
            $host ="127.0.0.1";
            $port ="8332";
            $url='';
            $CACertificate=null;
            $method=$body['method'];            
            // If no parameters are passed, this will be an empty array
            $params = $body['params'];
            $params = array_values($params);
            // The ID should be unique for each call
            $id++;
            // Build the request, it's ok that params might have any empty array
            $request = json_encode(array(
                'method' => $method,
                'params' => $params,
                'id'     => $id
            ));
            //$curl    = curl_init("{$proto}://{$host}:{$port}/{$url}");
            $curl    = curl_init("{$proto}://{$host}:{$port}/");
            $options = array(
                CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
                CURLOPT_USERPWD        => $username . ':' . $password,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_HTTPHEADER     => array('Content-type: application/json'),
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $request
            );
            // This prevents users from getting the following warning when open_basedir is set:
            // Warning: curl_setopt() [function.curl-setopt]:
            //   CURLOPT_FOLLOWLOCATION cannot be activated when in safe_mode or an open_basedir is set
            if (ini_get('open_basedir')) {
                unset($options[CURLOPT_FOLLOWLOCATION]);
            }
            
            if ($proto == 'https') {
               // If the CA Certificate was specified we change CURL to look for it
               if (!empty($CACertificate)) {
                   $options[CURLOPT_CAINFO] = $CACertificate;
                   $options[CURLOPT_CAPATH] = DIRNAME($CACertificate);
               } else {
                   // If not we need to assume the SSL cannot be verified
                   // so we set this flag to FALSE to allow the connection
                   $options[CURLOPT_SSL_VERIFYPEER] = false;
               }
           }
            curl_setopt_array($curl, $options);
            // Execute the request and decode to an array
            $raw_response = curl_exec($curl);
            $response     = json_decode($raw_response, true);
            // If the status is not 200, something is wrong
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // If there was no error, this will be an empty string
            $curl_error = curl_error($curl);
            curl_close($curl);
            if (!empty($curl_error)) {
                $error = $curl_error;
            }
            if ($response['error']) {
                // If EINR returned an error, put that in $error
                $error = $response['error']['message'];
            } elseif ($status != 200) {
                // If EINR didn't return a nice error message, we need to make our own
                switch ($status) {
                    case 400:
                        $error = 'HTTP_BAD_REQUEST';
                        break;
                    case 401:
                        $error = 'HTTP_UNAUTHORIZED';
                        break;
                    case 403:
                        $error = 'HTTP_FORBIDDEN';
                        break;
                    case 404:
                        $error = 'HTTP_NOT_FOUND';
                        break;
                }
            }
            if ($error) {
                return false;
            }
             //dd($response);
            return $response['result'];
        }catch(Exception $e){

        }
    }



    public function userimport(){
        return view('userimport');
    }

    public function userimportstore(Request $request){
            
        $filename=$_FILES["user"]["tmp_name"];      
        //echo $filename;
        //dd($request);        
        $n=array();
        if($_FILES["user"]["size"] > 0)
        {
            $file = fopen($filename, "r");
            $count=0;
            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
            {
 
                /*echo "<pre>";
                print_r($getData);
                echo "</pre>";*/

                $address=$getData[1];
                $count++;                
                $user=User::where('email','=',$address)->first();

                if(count($user)==0){

                    echo $address."<br>";
                     $name=$getData[0];
                     $pwd=$getData[2]; 

                     $user1=new User;                    
                     $user1->name=$name;
                     $user1->email=$address;
                     $user1->password=$pwd;
                     $user1->save(); 
                }else{
                    array_push($n, $count);
                }
              
            }
            fclose($file); 
        }
        //die;
        if(count($n)>0){
            $temp=implode("<br>", $n);
            $html="Existing user records...<br>".$temp;
            return back()->with('flash_error', $html);
        }else{
            return back()->with('flash_success', "Data Uploaded Successfully");
        } 
    }

    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {

        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    public function emailtokengenerate() {

        $users=User::where('email_token','=',null)->get();

        foreach ($users as $value) {
            $user=User::findOrFail($value->id);
            $user->email_token=base64_encode($value['email']);
            $user->save();
        }

        echo "done";

    }

    public function offerpop() {


        $transactions=TransactionHistory::get();
        //dd($transactions);
        $trans_count=$transactions->count();
        $rand=rand(1, $trans_count);

        $transaction=TransactionHistory::findOrFail($rand);

        $html='';
        if($transaction){

            if($transaction->payment_mode == 'BTC'){
                $html='<p class="offerpop_content_para"><a target="_blank" href="https://blockchain.info/tx/'.$transaction->payment_id.'">'.$transaction->price. Setting::get('coin_symbol').' bought by '.$transaction->payment_mode.'</a></p>';
            }    
            elseif($transaction->payment_mode == 'ETH'){
                $html='<p class="offerpop_content_para"><a target="_blank" href="https://etherscan.io/tx/'.$transaction->payment_id.'">'.$transaction->price.Setting::get('coin_symbol').' bought by '.$transaction->payment_mode.'</a></p>';
            }
            elseif($transaction->payment_mode == 'XRP'){

                $html='<p class="offerpop_content_para"><a target="_blank" href="https://xrpcharts.ripple.com/#/transactions/'.$transaction->payment_id.'">'.$transaction->price.Setting::get('coin_symbol').' bought by '.$transaction->payment_mode.'</a></p>';
            }
            
        
            return $html;
        
        }else{
            return 0;
        }
        

    }

}
