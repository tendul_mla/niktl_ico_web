@extends('layouts.app')

@section('content')

            <div class="user_details">
                <div class="container">

                    <!-- <div class="transaction_balance">
                        <div class="section-title">
                            <h1>Your ETH wallet</h1>
                            <h4>To receive {{ico()}} you'll need an ERC20-compliant ETH wallet.</h4>
                        </div>
                        <form action="{{url('/address')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="wallet_address" value="{{Auth::user()->coin_address}}">
                                <p>Don't use your exchange wallets for buying {{ico()}}.Use personal wallet only! Your ETH address must start with "0x", eg.: "0x1316f35873d5df1661719b9d1598d9ea29b7af4c".</p>
                            </div>
                            <div class="text-center common-button">
                                <button type="submit" class="btn btn-primary btn-info-full next-step">Save changes</button>
                            </div>
                        </form>
                    </div> -->

                     <div class="transaction_balance">
                        <div class="section-title">
                            <h1>@lang('user.common.eth_address')</h1>
                            <h4>@lang('user.profile.to_receive') {{ico()}} @lang('user.common.erc_addresscontent1') <a href="https://www.myetherwallet.com/" target="_blank">MyEtherWallet.com</a>.
                            </h4>
                        </div>
                        <form class="personal-details" action="{{url('ercaddressupdate')}}" method="post">
                            {{csrf_field()}}
                            <div class="input-group mt-30 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.common.eth_address')</label>
                                <input type="text" name="address" autocomplete="off" value="{{Auth::user()->erc_address}}">
                            </div>
                           
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center common-button">
                                <button type="submit" class="btn btn-primary btn-info-full next-step">@lang('user.common.erc_addresscontent2')</button>
                            </div>
                        </form>
                    </div>

                    <div class="transaction_balance">
                        <div class="section-title">
                            <h1>@lang('user.profile.personal_info')</h1>
                            <h4>@lang('user.profile.profile_content')</h4>
                        </div>
                        <form class="personal-details" action="{{url('/profile')}}" accept-charset="UTF-8" method="post">
                            {{csrf_field()}}
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.profile.full_name')</label>
                                <input type="text" name="name" value="{{Auth::user()->name}}" disabled>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.profile.email')</label>
                                <input type="email" name="email" value="{{Auth::user()->email}}" disabled=""> 
                            </div>
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Phone Number</label>
                                <input type="text" name="mobile" value="{{Auth::user()->mobile}}">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="disabled">BTC Wallet</label>
                                <input type="text" disabled value="{{balance(Auth::user()->BTC)}}">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="disabled">ETH Wallet</label>
                                <input type="text" disabled value="{{balance(Auth::user()->ETH)}}">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="disabled">XRP Wallet</label>
                                <input type="text" disabled value="{{balance(Auth::user()->XRP)}}">
                            </div> -->
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center common-button">
                                <button type="submit" class="btn btn-primary btn-info-full next-step">Save changes</button>
                            </div> -->
                        </form>
                    </div>

                    <div class="transaction_balance">
                        <div class="section-title">
                            <h1>@lang('user.profile.update_password')</h1>
                            <h4>@lang('user.profile.profile_content1')</h4>
                        </div>
                        <form class="personal-details" action="{{url('change/password')}}" method="post">
                            {{csrf_field()}}
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.password.current_password')</label>
                                <input type="password" name="old_password" autocomplete="off">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.password.enter_new_password')</label>
                                <input type="password" name="password">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.password.confirm_password')</label>
                                <input type="password" name="password_confirmation">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center common-button">
                                <button type="submit" class="btn btn-primary btn-info-full next-step">@lang('user.profile.save_changes')</button>
                            </div>
                        </form>
                    </div>

                  <!--   <div class="transaction_balance">
                        <form id="kyc">
                            <h4>Account status: 
                                @if(Auth::user()->status)
                                    <span class="success">Confirmed</span>
                                @else
                                    <span>Unconfirmed</span>
                                @endif
                            </h4>
                            <p>To confirm your account you will need to go though KYC proccess</p>
                            <div class="text-center common-button">
                                <a href="{{ url('/kyc') }}">
                                    <button type="button" class="btn btn-primary btn-info-full next-step">Start KYC Process</button>
                                </a>
                            </div>
                        </form>
                    </div> -->

                </div>
            </div>

@endsection