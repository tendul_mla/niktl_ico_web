<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ Setting::get('site_title') }}</title>
<link rel="shortcut icon" type="image/png" href="{{ img(Setting::get('site_icon')) }}"/>
<link rel="icon" href="{{ Setting::get('site_icon') }}" sizes="16x16" type="image/png">
<!-- Font Awesome CSS -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Ionicons CSS -->
<link href="ionicons/css/ionicons.min.css" rel="stylesheet">
<!-- Materail Design CSS -->
<link href="material-icons/css/materialdesignicons.min.css" rel="stylesheet">
<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<!-- lib -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- data table -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link href="{{ asset('css/animate.css') }}" rel="stylesheet">

<!-- custom style -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@yield('styles')
<style type="text/css">

</style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="logo navbar-brand" href="{{ url('/') }}">
                       <img src="{{ url('/storage') }}/{{ Setting::get('site_logo') }}" height="50" alt="{{ Setting::get('site_title') }}">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">@lang('user.common.login')</a></li>
                            <li><a href="{{ route('register') }}">@lang('user.common.register')</a></li>
                        @else
                            <li><a href="{{ url('/home') }}">@lang('user.common.home')</a></li>
                            <li><a href="{{ url('transaction') }}">@lang('user.common.transaction')</a></li>
                            <li><a href="{{ url('/referral') }}">@lang('user.common.referral')</a></li>
                           
                            <li><a href="{{ url('/kyc') }}">@lang('user.common.kyc')</a></li>
                            
                            <li><a href="{{ url('/profile') }}">@lang('user.common.profile')</a></li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            @lang('user.common.logout')
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="GET" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content-wrapper">
            @include('common.notify')
            @yield('content')
        </div>
        <style type="text/css">
            .effect5
            {
              
              background: #195260; height: 70px; width: 350px; position: fixed; bottom: 20px;left: 5px;
              /*box-shadow: 0 1px 15px 1px rgba(52,40,104,0.08);*/
              /*box-shadow: 11px 4px 25px 9px rgba(52,40,104,0.08);*/
              box-shadow: 0 0 10px rgba(0, 0, 0, .2);
              border: 1px solid #ccc;
              border-radius: 4px;
            }
            .offerpop_close{
              position: absolute;right: 5px;top:5px;                            
              background: #fff !important;
              background-size: 200% auto;
              font-weight: 600;
              transition: .5s;
              color: #000;
              border: 0 none;
              padding: 3px 8px;
              border-radius: 100px;
              font-size: 10px;
              line-height: 17px;
            }
            .offerpop_content {
                padding: 20px 30px;
                font-size: 20px;
            }
            .offerpop_content a{
              color: #fff;
            }

        </style>
        <div class="offerpop effect5" id="offerpop" style="display: none;">
          <a href="javascript:void(0);" class="offerpop_close" id="offerpop_close" onclick="myfuncofferpop_close();" >X</a>
          <div id="offerpop_content" class="offerpop_content"></div>
        </div>

       
    </div>
    <!-- jquery.min.js -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- Upload Doc -->
    <script type="text/javascript" src="{{ asset('js/jquery.uploadPreview.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }} "></script>
    <!-- Data trable -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready( function () {
        $('#myTable').DataTable();

        //offerpop();

      });
      $(document).ready( function () {
        $('#myTable1').DataTable();
      });

      function myfuncofferpop_close(){
        $("#offerpop").hide().fadeOut();
      }
      function offerpop(){
        $("#offerpop_content").html('');
        $.ajax({
          url: "{{url('/offerpop')}}",
          type: "GET",
          //data:{'usd':$('#priceusd').val(),'price':$('.price').val()}
        }).done(function(response){
          //console.log(response);
          //$('#offerpop').css('display','block');
          if(response!=0){
            $("#offerpop_content").html(response);
            $("#offerpop").show().delay(3000).fadeOut();
            /*$("#offerpop").toggle('slide', {
                direction: 'left'
            }, 500).fadeOut();*/
          }

        }).fail(function(jqXhr,status){

        });

        var minNumber = 10000;
        var maxNumber = 30000;

        var randomNumber = randomNumberFromRange(minNumber, maxNumber);

        //console.log(randomNumber);
        //setTimeout('offerpop()', 5000);
        setTimeout('offerpop()', randomNumber);
      }

      function randomNumberFromRange(min,max)
      {
          return Math.floor(Math.random()*(max-min+1)+min);
      }

    </script>

    <script src="{{ asset('js/scripts.js') }} "></script>

    <!-- Start of LiveChat (www.livechatinc.com) code -->
   <!-- <script type="text/javascript">
       window.__lc = window.__lc || {};
       window.__lc.license = 9721130;
       (function() {
         var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
         lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
     })();
 </script> -->

<!-- Start of beldex Zendesk Widget script -->
<!-- <script>/*<![CDATA[*/window.zE||(function(e,t,s){var n=window.zE=window.zEmbed=function(){n._.push(arguments)}, a=n.s=e.createElement(t),r=e.getElementsByTagName(t)[0];n.set=function(e){ n.set._.push(e)},n._=[],n.set._=[],a.async=true,a.setAttribute("charset","utf-8"), a.src="https://static.zdassets.com/ekr/asset_composer.js?key="+s, n.t=+new Date,a.type="text/javascript",r.parentNode.insertBefore(a,r)})(document,"script","45d37b36-8aec-4746-aff4-9e19aa08af66");/*]]>*/</script> -->
<!-- End of beldex Zendesk Widget script -->

 <script>
     function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  // console.log(Date.parse(new Date()));
  // console.log(Date.parse(endtime));
  // console.log(t);
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date()) + 1 * 2 * 04 * 54 * 1000);
// console.log(deadline);
initializeClock('clockdiv', deadline);
 </script>
 <script>
     document.querySelector("html").classList.add('js');

var fileInput  = document.querySelector( ".input-file" ),  
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");
      
button.addEventListener( "keydown", function( event ) {  
    if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        fileInput.focus();  
    }  
});
button.addEventListener( "click", function( event ) {
   fileInput.focus();
   return false;
});  
fileInput.addEventListener( "change", function( event ) {  
    the_return.innerHTML = this.value;  
});  
 </script>
 <script>
   
 </script>
   <!-- End of LiveChat code -->
    @yield('scripts')

</body>
</html>