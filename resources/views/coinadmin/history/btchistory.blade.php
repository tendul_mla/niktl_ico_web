@extends('coinadmin.layout.base')

@section('title', 'History')

@section('content')
<style type="text/css">
    .approve-test{display: none;}
</style>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
         
            <h3>BTC Transaction History</h3>

            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Address</th>
                        <th>{{ ico() }} Choosed Quantity</th>
                        <th>{{ ico() }} Actual Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Transactions as $index => $history)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                         <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td>
                        <td>{{ $history->user->name }}</td>
                        <td>{{ $history->user->email }}</td>
                        <td>

                        <?php $avb_amt = str_pad($history->id, 5, '0', STR_PAD_LEFT); ?>

                        {{Setting::get('coin_symbol')}}543{{ $avb_amt }}</td>
                        <td>
                            @if($history->payment_mode == 'BTC')
                                <a target="_blank" href="https://blockchain.info/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'ETH')
                                <a target="_blank" href="https://etherscan.io/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'XRP')
                                <a target="_blank" href="https://xrpcharts.ripple.com/#/transactions/{{ $history->payment_id }}">
                            @endif
                                {{substr($history->payment_id, 0, 8).'****'}}
                            </a>
                        </td>
                        <td>
                        	<a target="_blank" href="https://blockchain.info/address/{{$history->address}}">{{substr($history->address, 0, 8).'****'}}</a>
                        </td>
                        <td>{{ $history->ico_initial }}</td>
                        <td>{{ $history->ico }}</td>
                        <td>{{ currency($history->ico_price) }}</td>
                        <td>{{ $history->price }}</td>
                        @if($history->status == "pending")
                        <td style="color: #fc8019;">{{ $history->status }}</td>
                        @elseif($history->status == "processing")
                        <td style="color: #CCCC00;">{{ $history->status }}</td>
                        @elseif($history->status == "success")
                        <td style="color: #008000;">{{ $history->status }}</td>
                        @elseif($history->status == "failed")
                        <td style="color: #FF0000;">{{ $history->status }}</td>
                        @endif

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Address</th>
                        <th>{{ ico() }} Choosed Quantity</th>
                        <th>{{ ico() }} Actual Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
