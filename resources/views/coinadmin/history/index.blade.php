@extends('coinadmin.layout.base')

@section('title', 'History')

@section('content')
<style type="text/css">
    .approve-test{display: none;}
</style>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
         
            <h3> Transaction History</h3>

            <div class="filters">
                <div class="row" id="search">
                    <form id="search-form" action="{{ route('coinadmin.history.search') }}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group col-sm-3 col-xs-6">
                            <select data-filter="make"  class="filter-make filter form-control" name="payment_mode">
                                <option @if(isset($Request['payment_mode'])) @if($Request['payment_mode'] == 'ALL') selected="selected" @endif @endif value="ALL">ALL</option>
                                <option @if(isset($Request['payment_mode'])) @if($Request['payment_mode'] == 'BTC') selected="selected" @endif @endif value="BTC">BTC</option>
                                <option @if(isset($Request['payment_mode'])) @if($Request['payment_mode'] == 'ETH') selected="selected" @endif @endif value="ETH">ETH</option>
                                <option @if(isset($Request['payment_mode'])) @if($Request['payment_mode'] == 'XRP') selected="selected" @endif @endif value="XRP">XRP</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <select data-filter="model" class="filter-model filter form-control" name="referral">
                                <option @if(isset($Request['referral'])) @if($Request['referral'] == 'ALL') selected="selected" @endif @endif value="ALL">ALL</option>
                                <option @if(isset($Request['referral'])) @if($Request['referral'] == 'REFERRAL') selected="selected" @endif @endif value="REFERRAL">Referral</option>
                                <option @if(isset($Request['referral'])) @if($Request['referral'] == 'NON-REFERRAL') selected="selected" @endif @endif value="NON-REFERRAL">Un-Referral</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                           <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="date" value="@if(isset($Request['from_date'])) {{$Request['from_date']}} @endif" />
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <input class="form-control" id="to_date" name="to_date" placeholder="To Date" type="date" value="@if(isset($Request['to_date'])) {{$Request['to_date']}} @endif" />
                        </div>
                    
                        
                        <div class="form-group col-xs-9">
                            <input class="form-control" type="text" placeholder="Transaction ID" name="tx_id" value="@if(isset($Request['tx_id'])) {{$Request['tx_id']}} @endif" />
                        </div>
                        <div class="form-group col-xs-3">
                            <button type="submit" class="btn btn-block btn-primary">Search</button>
                        </div>

                    </form>
                </div>
            </div>

            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Payment Method</th>
                        <th>{{ ico() }} Choosed Quantity</th>
                        <th>{{ ico() }} Actual Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Action</th>
                        <!-- <th>Achieve</th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($History as $index => $history)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                         <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td>
                        <td>{{ $history->user->name }}</td>
                        <td>{{ $history->user->email }}</td>
                        <td>

                        <?php $avb_amt = str_pad($history->id, 5, '0', STR_PAD_LEFT); ?>

                        {{Setting::get('coin_symbol')}}543{{ $avb_amt }}</td>
                        <td>
                            @if($history->payment_mode == 'BTC')
                                <a target="_blank" href="https://blockchain.info/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'ETH')
                                <a target="_blank" href="https://etherscan.io/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'XRP')
                                <a target="_blank" href="https://xrpcharts.ripple.com/#/transactions/{{ $history->payment_id }}">
                            @endif
                                {{substr($history->payment_id, 0, 8).'****'}}
                            </a>
                        </td>
                        <td>{{ $history->payment_mode }}</td>
                        <td>{{ $history->ico_initial }}</td>
                        <td>{{ $history->ico }}</td>
                        <td>{{ currency($history->ico_price) }}</td>
                        <td>{{ $history->price }}</td>
                        @if($history->status == "pending")
                        <td style="color: #fc8019;">{{ $history->status }}</td>
                        @elseif($history->status == "processing")
                        <td style="color: #CCCC00;">{{ $history->status }}</td>
                        @elseif($history->status == "success")
                        <td style="color: #008000;">{{ $history->status }}</td>
                        @elseif($history->status == "failed")
                        <td style="color: #FF0000;">{{ $history->status }}</td>
                        @endif
                       
                        <td>
                            @if(($history->payment_mode == "BTC" || $history->payment_mode == "ETH" || $history->payment_mode == "EUR" || $history->payment_mode == "USD") &&  $history->status != "success")
                            <div class="input-group-btn">
                               
                                <button type="button" 
                                    class="btn btn-info btn-block dropdown-toggle"
                                    data-toggle="dropdown">Action
                                    <span class="caret"></span>
                                </button>

                                <ul class="dropdown-menu">

                                    <li>
                                        <a href="{{ route('coinadmin.history.success', $history->id ) }}" class="btn btn-default" style="color: #008000;"> Success </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('coinadmin.history.failed', $history->id ) }}" class="btn btn-default" style="color: #FF0000;"> Failed </a>
                                    </li>
                                     
                                </ul>

                            </div> 
                            @else
                            <span class="btn btn-default" style="color: #008000;">RECEIVED</span>
                            @endif

                            @if($history->wallet_add_status==0)
                               <a href="{{url('/addtorealwallet')}}/{{$history->id}}" class="btn btn-success approve-test" title="Approve to add in wallet" >Approve</a>
                            @endif
                        </td>
                        <!-- <td><a class="btn btn-default btn-block" href="{{ route('coinadmin.achieve', $history->id ) }}">Achieve</a></td> -->

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Payment Method</th>
                        <th>{{ ico() }} Choosed Quantity</th>
                        <th>{{ ico() }} Actual Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Action</th>
                        <!-- <th>Achieve</th> -->

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
