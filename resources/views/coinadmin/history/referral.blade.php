@extends('coinadmin.layout.base')

@section('title', 'History')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
         
            <h3>Referral Transaction History</h3>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Payment Method</th>
                        <th>{{ ico() }} Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Referred User</th>
                        <th>Referred Received</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($History as $index => $history)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                         <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td>
                        <td>{{ $history->user->name }}</td>
                        <td>{{ $history->user->email }}</td>
                        <td>

                        <?php $avb_amt = str_pad($history->id, 5, '0', STR_PAD_LEFT); ?>

                       {{Setting::get('coin_symbol')}}543{{ $avb_amt }}</td>
                        <td>
                            @if($history->payment_mode == 'BTC')
                                <a target="_blank" href="https://blockchain.info/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'ETH')
                                <a target="_blank" href="https://etherscan.io/tx/{{ $history->payment_id }}">
                            @elseif($history->payment_mode == 'XRP')
                                <a target="_blank" href="https://xrpcharts.ripple.com/#/transactions/{{ $history->payment_id }}">
                            @endif
                                {{substr($history->payment_id, 0, 8).'****'}}
                            </a>
                        </td>
                        <td>{{ $history->payment_mode }}</td>
                        <td>{{ $history->ico }}</td>
                        <td>{{ currency($history->ico_price) }}</td>
                        <td>{{ $history->price }}</td>
                     
                        <!-- <td><a class="btn btn-default btn-block" href="{{ route('coinadmin.achieve', $history->id ) }}">Achieve</a></td> -->
                        <td>@if($history->referral_by && isset($history->referred->email)) {{ $history->referred->email }} @else - @endif </td>
                        <td>@if($history->referral_amount) {{ $history->referral_amount }} @else - @endif </td>

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>ID</th>
                        <th>Date/Time</th>
                        <th>User</th>
                        <th>Email</th>
                        <th>Invoice</th>
                        <th>TXN</th>
                        <th>Payment Method</th>
                        <th>{{ ico() }} Quantity</th>
                        <th>{{ ico() }} Price</th>
                        <th>Price</th>
                        <th>Referred User</th>
                        <th>Referred Received</th>
                      

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
