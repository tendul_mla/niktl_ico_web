@extends('layouts.app')

@section('content')
     <div class="user_details">
                <div class="container">



                     <div class="transaction_balance">
                        <div class="section-title">
                            <h1>@lang('user.common.eth_address')</h1>
                            <h4>@lang('user.common.erc_addresscontent') {{ico()}} @lang('user.common.erc_addresscontent1') <a href="https://www.myetherwallet.com/" target="_blank">MyEtherWallet.com</a>.
                            </h4>
                        </div>
                        <form class="personal-details" action="{{url('ercaddressupdate')}}" method="post">
                            {{csrf_field()}}
                            <div class="input-group mt-30 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>@lang('user.common.eth_address')</label>
                                <input type="text" name="address" autocomplete="off" value="{{Auth::user()->erc_address}}">
                            </div>
                           
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center common-button">
                                <button type="submit" class="btn btn-primary btn-info-full next-step">@lang('user.common.erc_addresscontent2')</button>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
@endsection