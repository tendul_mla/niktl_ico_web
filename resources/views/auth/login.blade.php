@extends('layouts.login')

@section('content')
<style type="text/css">

</style>

<div class="login">
   <!--  <h4>Crowdfunding starts <b>May 2018</b></h4> -->
     
<div class="container">
        <div class="pull-right col-sm-8 col-lg-4">
            <div class="panel panel-default">
                <!-- <div class="panel-heading">Login</div> -->

                <div class="panel-body">
                    <a class="logo navbar-brand" href="{{ url('/') }}">
                       <img src="{{ url('/storage') }}/{{ Setting::get('site_logo') }}" height="50" alt="{{ Setting::get('site_title') }}">
                    </a>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}" id="form-login">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">@lang('user.login.email_address')</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">@lang('user.login.password')</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <div class="checkbox">
                                    <div class="g-recaptcha" data-sitekey="6LeKIHcUAAAAAPjcKFEzBCys2RVdjCRA2_cWl8SJ"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    @lang('user.common.login')
                                </button>

                                <p><a class="btn btn-link forget-password" href="{{ route('password.request') }}">
                                     @lang('user.login.forgot_password_login')
                                </a></p>
                            </div>
                        </div>

                        <div class="form-group bypass-pages">
                            <p>@lang('user.login.dont_have_account') <a href="{{url('/register')}}">@lang('user.common.register')</a></p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
</div>
</div>
@endsection

@section('scripts')
    <script>
    $("#form-login").submit(function(event) {

       var recaptcha = $("#g-recaptcha-response").val();
       if (recaptcha === "") {
          event.preventDefault();
          alert("Please check the recaptcha");
       }
    });
    </script>
@endsection