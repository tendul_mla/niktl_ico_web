@extends('layouts.app')

@section('content')


<div class="step_wizard">
    <div class="container">
        <div class="col-md-9 wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab">
                            <span class="step_count c1"><span class="step_span">@lang('user.home.step') </span>1</span> <span>@lang('user.home.payment_methods')</span>
                        </a>
                    </li> 
                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab">
                            <span class="step_count c2"><span class="step_span">@lang('user.home.step') </span> 2</span> <span>@lang('user.home.payment_setup')</span>
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab">
                            <span class="step_count c3"><span class="step_span">@lang('user.home.step') </span> 3</span> <span>@lang('user.home.transaction_detail')</span>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <div class="row">
                        @foreach($cointype as $cointypes)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center pay-method">
                            <input type="radio" name="coin-type" id="sell-{{$cointypes->id}}" value="{{$cointypes->symbol}}" style="display: none;"  onclick="mycoinselect(this.value);" >
                            <label for="sell-{{$cointypes->id}}">
                                @if($cointypes->symbol == 'ETH')
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="256px" height="417px" viewBox="0 0 256 417" version="1.1" preserveAspectRatio="xMidYMid">
                                    <g>
                                        <polygon points="127.9611 0 125.1661 9.5 125.1661 285.168 127.9611 287.958 255.9231 212.32" />
                                        <polygon points="127.962 0 0 212.32 127.962 287.959 127.962 154.158" />
                                        <polygon points="127.9611 312.1866 126.3861 314.1066 126.3861 412.3056 127.9611 416.9066 255.9991 236.5866" />
                                        <polygon points="127.962 416.9052 127.962 312.1852 0 236.5852" />
                                        <polygon points="127.9611 287.9577 255.9211 212.3207 127.9611 154.1587" />
                                        <polygon points="0.0009 212.3208 127.9609 287.9578 127.9609 154.1588" />
                                    </g>
                                </svg>
                                @elseif($cointypes->symbol == 'XRP')
                                <svg id="xrp" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.22 212.82">
                                    <path class="cls-1" d="M431,153.4c-7.52-4.34-16-5.6-24.39-5.9-7-.25-17.55-4.76-17.55-17.57,0-9.54,7.74-17.23,17.55-17.57,8.39-.29,16.86-1.55,24.39-5.9A44.45,44.45,0,1,0,364.31,68c0,8.61,3.06,16.54,7,23.89,3.29,6.18,4.95,17.66-6.32,24.16-8.39,4.84-18.85,1.78-24.08-6.59-4.42-7.07-9.75-13.69-17.21-18a44.45,44.45,0,1,0,0,77c7.46-4.31,12.79-10.92,17.21-18,3.62-5.8,12.67-13.19,24.08-6.6,8.37,4.84,11,15.44,6.32,24.17-3.91,7.35-7,15.27-7,23.88A44.45,44.45,0,1,0,431,153.4Z" transform="translate(-256.99 -23.53)"/>
                                </svg>
                                @elseif($cointypes->symbol == 'BTC')
                                <svg id="bitcoin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                    <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF" />
                                </svg>
                                @elseif($cointypes->symbol == 'BCH')
                                <svg id="bitcoin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                    <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF" />
                                </svg>
                                @elseif($cointypes->symbol == 'XRP')
                                            <svg id="xrp" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.22 212.82">
                                                <path class="cls-1" d="M431,153.4c-7.52-4.34-16-5.6-24.39-5.9-7-.25-17.55-4.76-17.55-17.57,0-9.54,7.74-17.23,17.55-17.57,8.39-.29,16.86-1.55,24.39-5.9A44.45,44.45,0,1,0,364.31,68c0,8.61,3.06,16.54,7,23.89,3.29,6.18,4.95,17.66-6.32,24.16-8.39,4.84-18.85,1.78-24.08-6.59-4.42-7.07-9.75-13.69-17.21-18a44.45,44.45,0,1,0,0,77c7.46-4.31,12.79-10.92,17.21-18,3.62-5.8,12.67-13.19,24.08-6.6,8.37,4.84,11,15.44,6.32,24.17-3.91,7.35-7,15.27-7,23.88A44.45,44.45,0,1,0,431,153.4Z" transform="translate(-256.99 -23.53)"/>
                                            </svg>
                                @elseif($cointypes->symbol == 'EUR')
                                            <svg version="1.1" id="euro" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14.994 14.994" style="enable-background:new 0 0 14.994 14.994;" xml:space="preserve">
                                                <g>
                                                    <path d="M12.774,11.67c-0.47,0.246-1.519,0.58-2.543,0.58c-1.116,0-2.166-0.334-2.879-1.139c-0.336-0.377-0.581-0.893-0.737-1.562
                                                        h5.602V7.965H6.304c0-0.111,0-0.222,0-0.357c0-0.246,0-0.467,0.021-0.692h5.893V5.332H6.661c0.158-0.58,0.378-1.093,0.715-1.45
                                                        c0.69-0.827,1.674-1.206,2.723-1.206c0.979,0,1.918,0.291,2.498,0.536l0.623-2.543C12.416,0.313,11.213,0,9.873,0
                                                        C7.754,0,5.968,0.847,4.72,2.299c-0.713,0.803-1.271,1.83-1.54,3.034H1.684v1.583h1.251c0,0.225-0.023,0.447-0.023,0.67
                                                        c0,0.133,0,0.27,0,0.379H1.684V9.55h1.452c0.201,1.185,0.646,2.142,1.249,2.9c1.251,1.651,3.235,2.544,5.443,2.544
                                                        c1.429,0,2.724-0.424,3.482-0.846L12.774,11.67z"/>
                                                </g>
                                            </svg>
                                            @elseif($cointypes->symbol == 'USD')
                                            <svg version="1.1" id="USD" 
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="79.536px" height="79.536px" viewBox="0 0 79.536 79.536" style="enable-background:new 0 0 79.536 79.536;"
                                                 xml:space="preserve">
                                                <g>
                                                    <path d="M36.081,60.766V42.145c-6.892-1.662-11.94-4.173-15.159-7.537
                                                        c-3.213-3.366-4.826-7.449-4.826-12.249c0-4.868,1.82-8.951,5.456-12.252c3.643-3.304,8.479-5.209,14.529-5.706V0h7.643v4.401
                                                        c5.588,0.557,10.036,2.162,13.334,4.821c3.309,2.651,5.411,6.198,6.333,10.641l-13.344,1.468c-0.803-3.5-2.915-5.864-6.323-7.109
                                                        v17.38c8.436,1.926,14.184,4.422,17.233,7.498c3.065,3.065,4.588,6.996,4.588,11.801c0,5.364-1.927,9.89-5.773,13.561
                                                        c-3.843,3.682-9.202,5.93-16.048,6.758v8.316h-7.643v-8.088C30,70.815,25.062,68.905,21.267,65.7
                                                        c-3.798-3.2-6.219-7.716-7.275-13.551l13.756-1.248c0.562,2.366,1.616,4.422,3.169,6.136
                                                        C32.459,58.761,34.184,60.004,36.081,60.766z M36.081,14.084c-2.079,0.598-3.733,1.61-4.958,3.027
                                                        c-1.225,1.421-1.846,2.99-1.846,4.714c0,1.569,0.557,3.027,1.688,4.373c1.124,1.359,2.825,2.439,5.111,3.267V14.084H36.081z
                                                         M43.724,61.392c2.631-0.409,4.774-1.439,6.422-3.086c1.656-1.657,2.479-3.583,2.479-5.81c0-1.979-0.698-3.697-2.076-5.137
                                                        c-1.388-1.43-3.661-2.538-6.825-3.294V61.392z"/>
                                                </g>
                                            </svg>

                                @endif
                                <h4>{{$cointypes->symbol}}</h4>
                                <p><span>@lang('user.home.get') {{ico()}} @lang('user.home.coins_using'){{$cointypes->symbol}}</span></p>
                                
                               
                            </label>
                        </div>
                        @endforeach

                       
                    </div>
                    <ul class="list-inline">
                        <li class="pull-right">
                            <button type="button" disabled="disabled" class="btn btn-primary step-1 next-step"> @lang('user.home.continue')</button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">
                    <div class="step2_contents">
                        <h4>
                            <svg id="bitcoin" class="BTC symbol" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF"/>
                            </svg>
                            <svg id="litecoin" class="ETH symbol" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="256px" height="417px" viewBox="0 0 256 417" version="1.1" preserveAspectRatio="xMidYMid">
                                <g>
                                    <polygon points="127.9611 0 125.1661 9.5 125.1661 285.168 127.9611 287.958 255.9231 212.32" />
                                    <polygon points="127.962 0 0 212.32 127.962 287.959 127.962 154.158" />
                                    <polygon points="127.9611 312.1866 126.3861 314.1066 126.3861 412.3056 127.9611 416.9066 255.9991 236.5866" />
                                    <polygon points="127.962 416.9052 127.962 312.1852 0 236.5852" />
                                    <polygon points="127.9611 287.9577 255.9211 212.3207 127.9611 154.1587" />
                                    <polygon points="0.0009 212.3208 127.9609 287.9578 127.9609 154.1588" />
                                </g>
                            </svg>
                            <svg id="xrp" class="XRP symbol" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.22 212.82">
                                <path class="cls-1" d="M431,153.4c-7.52-4.34-16-5.6-24.39-5.9-7-.25-17.55-4.76-17.55-17.57,0-9.54,7.74-17.23,17.55-17.57,8.39-.29,16.86-1.55,24.39-5.9A44.45,44.45,0,1,0,364.31,68c0,8.61,3.06,16.54,7,23.89,3.29,6.18,4.95,17.66-6.32,24.16-8.39,4.84-18.85,1.78-24.08-6.59-4.42-7.07-9.75-13.69-17.21-18a44.45,44.45,0,1,0,0,77c7.46-4.31,12.79-10.92,17.21-18,3.62-5.8,12.67-13.19,24.08-6.6,8.37,4.84,11,15.44,6.32,24.17-3.91,7.35-7,15.27-7,23.88A44.45,44.45,0,1,0,431,153.4Z" transform="translate(-256.99 -23.53)"/>
                            </svg>

                            <span><span class="payment"></span> @lang('user.home.payment') (1 {{ico()}} = {{ Setting::get('coin_price') }} USD)</span>
                        </h4>
                        <ul class="calculation_ul">
                            <li>
                                <div class="form-group">

                                    <label>{{ico()}} @lang('user.home.qty_buy')</label>

                                    <input id="ico" type="number" step="any" name="quantity" placeholder="Min. 100 {{ico()}}" min="100">
                                </div>
                            </li>
                            <li class="hidden-xs">
                                <span><i class="fa fa-arrow-circle-right" style="font-size:48px;color:#9aca3c"></i></span>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>@lang('user.home.equivalent')  <span class="payment"></span></label>
                                    <input class="price" type="number" step="any" name="">
                                </div>
                            </li>
                        </ul>
                        <ul class="calculation_result">
                            <li>
                                <p><input id="promocode" type="hidden" name="promocode" placeholder="Promocode"><span class="promo-txt no-promo"></span></p>
                            </li>
                            <li>
                                <p>@lang('user.mail.referral_trans_content2') = <span class="">&nbsp;USD</span> <span class="priceusd">0.00</span></p>
                                <!-- <input type="hidden" name="" id="priceusd" /> -->
                            </li>
                            <li>
                                <p>@lang('user.mail.referral_trans_content3') = <span class="payment"></span> <span class="price">0.00</span></p>
                            </li>
                            @if(count($bonuses)>0)
                            <li>
                                <p>{{$bonuses[0]->percentage}}% @lang('user.home.discount')  <span>&nbsp;{{ico()}}</span><span id="bonus">  0.00</span></p>
                                <input type="hidden" id="bonus_point" name="bonus_point" value="{{$bonuses[0]->percentage}}" />
                            </li>
                            @endif
                            <li>
                                <p class="final_bdx">@lang('user.mail.final') {{ico()}} = <span class="final_bdx">&nbsp;{{ico()}}</span> <span class="final_bdx" id="total">0.00  </span></p>
                            </li>
                        </ul>
                    </div>
                    <ul class="list-inline">
                        <li>
                            <button type="button" class="btn btn-default prev-step">@lang('user.home.bck_options')</button>
                        </li>
                        <li class="pull-right">
                            <button type="button" disabled="disabled" class="btn btn-primary step-2 next-step">@lang('user.home.nxt_stp')</button>
                        </li>
                    </ul>
                </div>

                <div class="tab-pane" role="tabpanel" id="step3">
                    <div class="tab-top">
                        <h4><span class="payment"></span> @lang('user.home.funds_only')</h4>
                        <p>@lang('user.home.send_only') <span class="payment"></span> @lang('user.home.home_content')<span>  @lang('user.home.home_content1').</span></p>
                       <!--  <p><span>Please note</span> that the address for this funding is unique and can only be used once.</p> -->
                        <p><span>@lang('user.home.exact') {{Setting::get('coin_symbol')}} @lang('user.home.home_content2').</p> 
                    </div>

                    <div>
                        <h6  id="pay">
                            <i class="fa fa-circle-o-notch fa-spin" style=""></i>
                            <span style="font-weight: 600;">@lang('user.home.home_content3').</span>
                        </h6>
                        <h6  id="payshow">
                            <i class="fa fa-circle-o-notch fa-spin"></i>
                            @lang('user.home.payment_received').
                        </h6>
                    </div>

                    <div class="QR-Code">
                        <p class="ico_launch"></p>
                        
                        <div id="clockdiv">
                            <div style="display: none;">
                                <span class="days"></span>
                                <div class="smalltext">@lang('user.home.days')</div>
                            </div>
                            <div style="display: none;">
                                <span class="hours"></span>
                                <div class="smalltext">@lang('user.home.hours')</div>
                            </div>
                            <div>
                                <span class="minutes"></span>
                                <div class="smalltext">@lang('user.home.minutes')</div>
                            </div>
                            <div>
                                <span class="seconds"></span>
                                <div class="smalltext">@lang('user.home.seconds')</div>
                            </div>
                        </div>
                        <div class="crypto_block">
                        <p>Send <span class="amount price">0.0000000</span><strong> <span class="payment"></span></strong> >@lang('user.home.to_address'):</p> 

                        @foreach($cointype as $cointypes)
                            <div class="{{$cointypes->symbol}} qr-code" style="display: none;">
                                <!-- <img class="img-responsive" src="{{ img($cointypes->qr_code) }}" width="200"> -->

                                @if($cointypes->symbol == "BTC")
                                <?php $address = $btc_addr = Auth::user()->btc_address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->btc_address}}"></div>
                                @elseif($cointypes->symbol == "BCH")
                                <?php $address = $bch_addr = $cointypes->address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->bch_address}}"></div>
                                @elseif($cointypes->symbol == "ETH")
                                <?php $address = $eth_addr = Auth::user()->eth_address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->eth_address}}"></div>
                                @else
                                <?php $address = $cointypes->address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{$cointypes->address}}"></div>
                                @endif


                                <div class="input-group mt-30">
                                    <label class="label">@lang('user.home.address')</label>
                                    <input type="text" placeholder="{{$cointypes->address}} Address" value="{{$address}}" id="address-{{$cointypes->address}}">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-theme" onclick="myFunction{{$cointypes->address}}()" >@lang('user.home.copy_clipboard')</button>
                                    </span>
                                    
                                </div>
                                <div class="my_alert" style="display: none;">
                                   @lang('user.home.home_content4').
                                </div>

                                @if($cointypes->symbol == "XRP")
                                <div class="input-group mt-30">
                                    <label class="label">@lang('user.buy.destination_tag')</label>
                                    <input type="text" value="<?php echo $dest_tag = Auth::user()->dest_tag; ?>" id="address-{{$cointypes->symbol}}">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-theme" onclick="myFunction{{$cointypes->symbol}}()" >@lang('user.home.copy_clipboard')</button>
                                    </span>
                                </div>
                                @endif

                                <script>
                                    function myFunction{{$cointypes->symbol}}() {
                                        var copyText = document.getElementById("address-{{$cointypes->symbol}}");
                                        copyText.select();
                                        document.execCommand("Copy");
                                        $('.my_alert').show();
                                    }
                                </script>
                                <script>
                                    function myFunction{{$cointypes->address}}() {
                                        var copyText = document.getElementById("address-{{$cointypes->address}}");
                                        copyText.select();
                                        document.execCommand("Copy");
                                        $('.my_alert').show();
                                    }
                                </script>
                            </div>
                            @if($cointypes->symbol == "XRP")
                                <?php 
                                $ripple_addr = $cointypes->address;
                                ?>
                                <br>
                                <!-- <h6  id="pay">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                    Awaiting Payment, Please don't close or refresh this window.
                                </h6>
                                <h6  id="payshow">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                    Payment received.
                                </h6> -->
                            @endif
                        @endforeach
                    </div>

                        <div class="fiat_euro">
                             <p>Send <span class="amount price">0.0000000</span>
                                <span class="fiat_euro_name"></span><strong> <span class="payment"></span></strong> </p>
                            @foreach($cointype as $cointypes)
                            @if($cointypes->symbol == "EUR")
                            {!!$cointypes->address!!}
                            @endif
                            @endforeach
                        </div>

                        <div class="fiat_USD">
                             <p>Send <span class="amount price">0.0000000</span><strong> <span class="payment"></span><span class="fiat_usd_name"></span></strong> </p>
                            @foreach($cointype as $cointypes)
                            @if($cointypes->symbol == "USD")
                            {!!$cointypes->address!!}
                            @endif
                            @endforeach
                         
                        </div>
                         <input id="wirevalue" type="hidden"  name="">

                        <form method="post" action="{{url('transaction')}}" id="transaction_form">
                            {{csrf_field()}}
                            <!-- <label>Transaction ID</label> -->

                            <input type="hidden"  id="amount_ctc"  name="amount_ctc" required="">
                            <input type="hidden" id="amount_new"  name="amount_new" required="">
                            <input type="hidden" id="promo_code"  name="promo_code" required="">
                            <input type="hidden" id="cointype"  name="cointype" required="">
                            <input type="hidden" id="coin_address"  name="coin_address" required="">
                            <input type="hidden" id="f"  name="referal_code" value="0">
                            <input type="hidden" id="address"  name="address">
                            <input type="hidden" name="priceusd" id="priceusd" />
                            <input type="hidden" name="selectedcoin" id="selectedcoin" />
                            <input type="text" class="form-control trans_input" placeholder="Transaction Id" name="tranx_id" id="tranx_id">

                            @if(count($bonuses)>0)
                            <input type="hidden"  name="bonus_point" value="{{$bonuses[0]->percentage}}" />
                            @else
                            <input type="hidden"  name="bonus_point" value="0" />
                            @endif



                            <!-- <div class="input-group">
                            <input type="text" placeholder="Transaction Id" name="tranx_id" id="tranx_id"> -->
                            <!--  <span class="">  -->
                                <button type="button" disabled="" class="fiat trans_btn btn btn-default "> @lang('user.home.proceed') </button>


                                <!-- <button type="button" id="btn-token-promocode-add-new" disabled="" class="step-3 btn btn-theme btn-green"> Proceed </button> -->

                                <!-- <button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" disabled="" data-backdrop="static" data-keyboard="false">Proceed</button> -->

                               <!--  <button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" >Proceed</button> -->

                                <!-- <div id="coin_type_btn_submit">
                                    
                                </div> -->
                                
                            <!-- </span>
                            </div> -->
                            <label style="color:red;display: block;" class="error_tranx_id"></label>
                        </form>
                        <!-- <p class="received-amount">Amount Received: <span class="amount price">0.000000</span><strong> <span class="payment"></span></strong></p> -->
                    </div>
                    <ul class="list-inline">
                        <li>
                            <button type="button" class="btn btn-default prev-step">@lang('user.home.bck_options')</button>
                        </li>
                        <li class="pull-right">

                            <!-- <button disabled="disabled" class="btn btn-primary step-4 btn-info-full next-step" onclick="window.location.href='{{ url('/transaction') }}'">CHECK MY TRANSACTIONS</button> -->

                            <!-- <a href="{{ url('/transaction') }}" disabled="disabled" class="btn btn-primary step-4 btn-info-full next-step" >CHECK MY TRANSACTIONS</a> -->
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
                </div>
                </div>
                <div class="col-md-3 side_wizard wizard" style="padding: 0px; background: #f5f5f5; height: auto;">
                    <div class="page">
                      <div class="page__demo">
                        <div class="main-container page__container">
                          <div class="timeline">
                            <div class="timeline__group">
                              <span class="timeline__year">@lang('user.home.step')</span>
                              <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">1</span>                              
                              </div>
                                  <div class="timeline__post">
                                      <div class="timeline__content">
                                        <p>@lang('user.home.home_content5'){{Setting::get('coin_symbol')}} @lang('user.home.coin').</p>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">2</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>@lang('user.home.home_content6') {{Setting::get('coin_symbol')}} @lang('user.home.home_content7').</p>
                                </div>
                            </div>
                        </div>
                        <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">3</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>@lang('user.home.home_content8') {{Setting::get('coin_symbol')}} @lang('user.home.home_content9').  </p>
                                </div>
                            </div>
                        </div>
                        <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">4</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>@lang('user.home.home_content0').</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>

</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang('user.home.confirmation')</h4>
      </div>
      <div class="modal-body">
        <p style="font-size: 1.5em; color: #555;text-align: center;">@lang('user.home.modal_content'). </p>
        <br>
        <p style="font-size: 1.5em; color: #555;text-align: center;">@lang('user.home.modal_content1') {{Setting::get('coin_symbol')}} @lang('user.home.modal_content2')...</p>
        
        <!--<p style="font-size: 1.5em; color: #555;text-align: center;">Your payment has been credited successfully, Once the payment have confirmed, your BDX Coin will be credited to your wallet... </p> -->
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-token-promocode-add-new" class="step-3 btn btn-theme btn-green">@lang('user.home.modal_content3')</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('user.home.close')</button>
      </div>
    </div>

  </div>
</div>

@endsection

@section('scripts')

<script>

    function mycoinselect(id){
        $('#selectedcoin').val(id);


        if(id == "BTC"){
            $('#coin_type_btn_submit').html('<button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" disabled="" >Proceed</button>');
        }else{
            $('#coin_type_btn_submit').html('<button type="button" id="btn-token-promocode-add-new" disabled="" class="step-3  btn btn-theme btn-green"> Proceed </button>');
        }
        
    }
    

    $(document).ready(function() {

        var payment, promo, ico, price;

        $('.pay-method input[type=radio]').change(function(){
           
            if(payment = $(this).val()) { //XRP Disable
          
                    $('.step-1').prop('disabled',false);
                
                //$('.payment').html(payment);
                $('.step2_contents > h4 > span > span.payment').html(payment);
                $('.qr-code, .symbol').hide();
                $('.'+payment).show();
                $('#ico').trigger('change');
                if($(this).val() == 'EUR' || $(this).val() == 'USD'){
                    $('.crypto_block').hide();
                    
                    if($(this).val() == 'EUR'){
                        $('.fiat_euro').show();
                       // alert('test');
                    }else{
                        $('.fiat_USD').show();
                       // alert('testing');
                    }
                    $('.fiat').show();
                }else{
                    $('.crypto_block').show();
                    $('.fiat_euro').hide();
                    $('.fiat_USD').hide();
                    $('.fiat').hide();
                }
            }

        });

        $("#ico, #promocode, .price").change(function(){
            promo = $('#promocode').val();
            ico = $('#ico').val();
            price = $('.price').val();
            if(ico || price) {

                id = $(this).attr('id');
                $.get("{{url('/coinstatus')}}?promo="+promo+"&type="+payment, function(data, status) {
                    if(data.message == 'OK') {
                        if(payment == 'ETH') {
                            var live_price = data.result.ethusd;
                        } else {
                            var live_price = data.last;
                        }
                        if(payment == 'EUR' ) {
                            
                            var bitcoin = (parseFloat(<?php echo Setting::get('wire_euro_value') ;?>) * ico).toFixed(8);
                            console.log(bitcoin);
                            $(".priceusd").html(parseFloat(bitcoin));
                            $("#priceusd").val(parseFloat(bitcoin));
                            $(".price").val(parseFloat(bitcoin));
                            $(".price").html(parseFloat(bitcoin));

                        }

                        else if(payment == 'USD' ) {
                            
                            var bitcoin = (parseFloat(<?php echo Setting::get('wire_usd_value') ;?>) * ico).toFixed(8);
                            console.log(bitcoin);
                            $(".priceusd").html(parseFloat(bitcoin));
                            $("#priceusd").val(parseFloat(bitcoin));
                            $(".price").val(parseFloat(bitcoin));
                            $(".price").html(parseFloat(bitcoin));

                        }

                        else if(id == 'ico') {
                            var bitcoinusd = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico).toFixed(8);
                            $(".priceusd").html(parseFloat(bitcoinusd));
                            $("#priceusd").val(parseFloat(bitcoinusd));
                            
                            var bitcoin = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico / live_price).toFixed(8);
                            $(".price").val(parseFloat(bitcoin));
                            $(".price").html(parseFloat(bitcoin));
                        } else {
                            ico = parseInt(live_price * price / <?php echo Setting::get('coin_price') ;?>)
                            $("#ico").val(ico);
                            $(".price").html(parseFloat(price));

                            var bitcoinusd = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico).toFixed(8);
                            $(".priceusd").html(parseFloat(bitcoinusd));
                            $("#priceusd").val(parseFloat(bitcoinusd));
                        }

                        var bonus_point = parseFloat($('#bonus_point').val());
                        var bonus = parseFloat(ico)*(bonus_point/100);
                        $("#bonus").html(parseFloat(bonus));
                        if(data.promo_percent) {
                            var promo_percent = parseFloat(ico)*(data.promo_percent/100);
                            $(".promo-txt").removeClass('no-promo');
                            $(".promo-txt").html('Promocode Applied');
                        } else {
                            var promo_percent = 0;
                        }
                        if(bonus_point) {
                            var total = parseFloat(ico)+parseFloat(bonus)+parseFloat(promo_percent);
                        } else {
                            var total = parseFloat(ico)+parseFloat(promo_percent);
                        }
                        $("#total").html(parseFloat(total));
                        
                        if(ico>=100){
                            $('.step-2').prop('disabled',false);
                        }else{
                            $('.step-2').prop('disabled',true);
                            alert("Amount of {{ico()}} to buy should be greater than or equal to 100");
                        }

                    } else {
                        if(data.error == 'invalid_promo'){
                            $(".promo-txt").html('Invalid Promocode');
                        }else if(data.error == 'promo_expired'){
                            $(".promo-txt").html('Promocode Expired');
                        }else if(data.error == 'promo_used'){
                            $(".promo-txt").html('Promocode already Used');
                        }
                        $(".promo-txt").addClass('no-promo');
                        $('#promocode').val('');
                        $('#ico').trigger('change');
                    }
                });
                
            }
        });

        $(".step-1").click(function(){
            $('.wiz1').hide();
            $('.wiz2').show();            
        });        

        $(".step-2").click(function(){
            var n=0;            
            $('#payshow').hide();
            if(payment == 'XRP'){
                $('.code-details').hide();
                $('#tranx_id').hide();
                checkTransaction();
            } else if(payment == 'BTC'){
                $('.code-details').hide();
                $('#tranx_id').hide();
                checkBTCTransaction();
            } else if(payment == 'ETH'){
                $('.code-details').hide();
                $('#tranx_id').hide();
                checkETHTransaction();
            }
            else if(payment == 'EUR'){
                $('.tab-top').hide();
                $('#tranx_id').show();
                $('#account_number').show();
                $('.code-details').show();
                $('.fiat_USD').hide();
                $('#transaction_form button').attr('disabled', false);
                $('.step-4').attr('disabled', false);
                $('#amount_ctc').val($('#ico').val());
                $('#amount_new').val($('.price').val());
                $('#promo_code').val($('#promocode').val());
                $('.fiat_euro_name').html(payment);
                $('#cointype').val(payment);
            } 
            else if(payment == 'USD'){
                $('.tab-top').hide();
                $('#tranx_id').show();
                $('#account_number').show();
                $('.code-details').show();
                $('.fiat_euro').hide();
                $('#transaction_form button').attr('disabled', false);
                $('.step-4').attr('disabled', false);
                $('#amount_ctc').val($('#ico').val());
                $('#amount_new').val($('.price').val());
                $('#promo_code').val($('#promocode').val());
                $('#cointype').val(payment);
                $('.fiat_USD_name').html(payment);
            }
             else {
                n=1;
                $('#tranx_id').show();
                $('#transaction_form button').attr('disabled', false);
                $('.step-3').attr('disabled', false);
            }

            $('.wiz2').hide();
            $('.wiz3').show();

            $('.QRImage').html('');
            
            var code = 'bitcoin:'+$('#qrcode-BTC').attr('ins')+'?amount='+$('.price').val();

            $('#qrcode-BTC').qrcode(code);

            var code = $('#qrcode-ETH').attr('ins');

            $('#qrcode-ETH').qrcode(code);

            var code = $('#qrcode-XRP').attr('ins');

            $('#qrcode-XRP').qrcode(code);


            if(n==0){

                $.ajax({
                  url: "{{url('/paymentsecondstep')}}",
                  type: "GET",
                  data:{'usd':$('#priceusd').val(),'price':$('.price').val(),'cointype':payment,'amount_ctc':$('#ico').val(),'ico':'{{ico()}}'}
              }).done(function(response){

              }).fail(function(jqXhr,status){

              });

          }

        });

        $('.trans_btn').on('click',function(){
            if($('#tranx_id').val() != ""){
                $('#transaction_form').submit();
            }else{
               alert('Enter Transaction Id');
            }
        });

        

        //$('.proceed').on('click',function(){
        $('.step-3').on('click',function(){
            $('#myModal').modal('hide');
            if(payment != 'EUR' &&  payment != 'USD'){
                myfunclaststep();  
            }
                      
        });

        // Last Step Functionality
        function myfunclaststep(){
            if($('#tranx_id').val()){
                var myVar;
                $.ajax({
                  url: "{{url('checkTransaction')}}",
                  type: "GET",
                  data:{'tranx_id':$('#tranx_id').val(),'amount':$('.price').val(),'amount_ctc':$('#ico').val(),'cointype':payment}
                  }).done(function(response){
                    if(response.success == 'Ok'){
                        $(".error_tranx_id").html('');
                        $('#step-3').prop('disabled',false);
                        $('#amount_ctc').val($('#ico').val());
                        if(!$('#amount_new').val()) {
                            $('#amount_new').val($('.price').val());
                        }
                        console.log(payment);
                        $('#promo_code').val($('#promocode').val());
                        $('#cointype').val(payment);
                        $('#transaction_form').submit();  // Save transaction
                        $('#payshow').show();
                    }else if(response.error == 'id_not_valid'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Invalid Transaction ID');
                    }else if(response.error == 'price_not_match'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Price does not match');
                    }else if(response.error == 'address_not_match'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Deposit Address does not match');
                    } else{
                        $('#loader').show();
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('Please wait until your Transaction is Processing...');

                    }

                }).fail(function(jqXhr,status){
                    if(jqXhr.status === 422) {
                        error = 1;
                        $(".error_tranx_id").html('');
                        $(".error_tranx_id ").show();
                        var errors = jqXhr.responseJSON;
                        console.log(errors);
                        $.each( errors , function( key, value ) {
                            $(".error_tranx_id").html(value);
                        });
                    }
                    $('#step-3').prop('disabled',true);
                });
            }else{
                $('.error_tranx_id').html('This field is required');
            }
        }

       
    

         // BTC Function
        var error_btc = 0;
        function checkBTCTransaction(){
            var amount_btc = $('#price').val();
            var add = '{{$btc_addr}}';
            $.ajax({
              url: "{{url('checkbtctran')}}",
              type: "GET",
              data:{'tx_id': add ,'btc': amount_btc}
            })
            .done(function(response){
                console.log(response);
                // alert(response);
                var transa =1;
                var amount_btc = $('.price').val();
                console.log(response);


                //if(parseFloat(response.txApperances) > 0){
                    transa =0; 
                    if(response.amount) {
                        var total = response.amount;
                    }
                        // } else {
                        //     var total = response.unconfirmedBalanceSat;
                        // }
                        // console.log(value.total/1000000000000000000 +'---'+ amount_btc+'--'+parseFloat(amount_btc - (value.total/1000000000000000000)));
                        if(total) {

                            // $.each(response.transactions ,function(index,value){
                               // console.log(response.transactions[0]);
                               var ids = response.txid;
                               var amount_new = response.amount;
                               var address = response.address;
                               // console.log(amount_btc);
                               // if(parseFloat(amount_btc - (total/100000000)) < 0.0005) {
                                $.ajax({
                                  url: "{{url('checkTrans')}}",
                                  type: "GET",
                                  data:{'tx_id': ids}
                                })
                                .done(function(response){
                                    console.log(response);
                                    if(response == '1') {
                                        setTimeout(function() { checkBTCTransaction(); }, 5000);
                                    } else {
                                        $('#tranx_id').val(ids);
                                        $('#amount_new').val(amount_new);
                                        $('#address').val(address);

                                        $('.fa').remove();
                                        $('#pay').hide();
                                        
                                        $('#payshow').show();

                                        $("#waiting-msg-Bitcoin").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                                        $('#btn-token-promocode-add-new').prop('disabled', false);
                                        //$('#myModal').modal('show');

                                        myfunclaststep();
                                    }
                                });

                                // }else{
                                //   setTimeout(function() { checkBTCTransaction(); }, 5000);
                                // }
                            // });
                        } else {
                            setTimeout(function() { checkBTCTransaction(); }, 5000);
                        }
                        if(transa==1){
                          setTimeout(function() { checkBTCTransaction(); }, 5000);
                        }

                // }
                //else{
                    //setTimeout(function() { checkBTCTransaction(); }, 5000);
                // }

            })
            .error(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error_eth =1;
                    $("#waiting-msg-Bitcoin").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Bitcoin").html(value);
                    }); 
                }
            })
        }
        
        
        // Etherium Function
        var error_eth = 0;
        function checkETHTransaction(){
            $.ajax({
              url: "https://api.etherscan.io/api?module=account&action=txlist&address={{$eth_addr}}&page=1&offset=1&sort=desc",
              type: "GET",
                  //data:{'payment_id':$('#transaction_id_'+type).val(),'amount':amount}
            })
            .done(function(response){
                console.log(response);
                var transa =1;
                var amount_eth = $('#price').val();
                if((response.result).length > 0){
                    console.log(amount_eth);
                    $.each(response.result ,function(index,value){

                        if(value.to == {{$eth_addr}}) {
                         transa =0;
                               // console.log(value.value/1000000000000000000 +'---'+ amount_eth+'--'+parseFloat(amount_eth - (value.value/1000000000000000000)));
                               var ids = value.hash;
                               // if(parseFloat(amount_eth - (value.value/1000000000000000000)) < 0.005) {
                                  $.ajax({
                                      url: "{{url('checkTrans')}}",
                                      type: "GET",
                                      data:{'tx_id': ids}
                                  }).done(function(response){
                                      if(response == '1') {
                                        setTimeout(function() { checkETHTransaction(); }, 5000);
                                    } else {
                                        $('#tranx_id').val(value.hash);
                                        $('#amount_new').val(value.value/1000000000000000000);
                                        $('#address').val(value.to);
                                        $('.fa').remove();
                                        $('#pay').hide();  
                                        $('#payshow').show();
                                        $("#waiting-msg-Ethereum").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                                        $('#btn-token-promocode-add-new').prop('disabled', false);
                                        //$('#myModal').modal('show');

                                        myfunclaststep();
                                    }
                                });
                                // }else{
                                //   setTimeout(function() { checkETHTransaction(); }, 5000);
                                // }
                            }
                        });

                    if(transa==1){
                      setTimeout(function() { checkETHTransaction(); }, 5000);
                  }

              }
              else{
                  setTimeout(function() { checkETHTransaction(); }, 5000);
              }

            })
            .error(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error_eth =1;
                    $("#waiting-msg-Ethereum").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Ethereum").html(value);
                    }); 
                }
            })
        }

        // Ripple Function
        var error = 0;
        function checkTransaction(){
            $.ajax({
                url: "https://data.ripple.com/v2/accounts/{{$ripple_addr}}/transactions?type=Payment&result=tesSUCCESS&limit=15",
                type: "GET",
                    //data:{'payment_id':$('#transaction_id_'+type).val(),'amount':amount}
            })
            .done(function(response){
                var transa =1;
                var amount_ripple = $('#price').val();
                if((response.transactions).length > 0){
                    console.log(response)
                    $.each(response.transactions ,function(index,value){
                        console.log(value.tx.DestinationTag);
                        if(value.tx.DestinationTag == {{$dest_tag}}){
                         transa =0;
                           // console.log(value.tx.Amount/1000000 +'---'+ amount_ripple+'--'+parseFloat(amount_ripple - (value.tx.Amount/1000000)));
                           // if(parseFloat(amount_ripple - (value.tx.Amount/1000000)) < 2){
                            $('#tranx_id').val(value.hash);
                            $('#amount_new').val(value.tx.Amount/1000000);
                            $('#address').val(value.tx.DestinationTag);
                            $('.fa').remove();
                            $('#pay').hide();  
                            $('#payshow').show();
                            $("#waiting-msg-Ethereum").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                            $('#btn-token-promocode-add-new').prop('disabled', false);
                            //$('#myModal').modal('show');

                            myfunclaststep();

                            // }else{
                            //     $("#waiting-msg-Ripple").html('<i class="fa fa-close"></i> Paid amount is less than the order amount. Please re-pay the exact amount.');
                            // }
                        }
                    });

                    if(transa==1){
                      setTimeout(function() { checkTransaction(); }, 5000);
                  }

              }
              else{
                  setTimeout(function() { checkTransaction(); }, 5000);
              }

            })
            .fail(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error =1;
                    $("#waiting-msg-Ripple").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Ripple").html(value);
                    }); 
                }
            })
        }

        


        // Steps Function
        $(".c1").click(function(){
            $('.wiz1').show();
            $('.wiz2').hide();
            $('.wiz3').hide();
        });

        $(".c2").click(function(){
            $('.wiz1').hide();
            $('.wiz2').show();
            $('.wiz3').hide();
        });

        $(".c3").click(function(){
            $('.wiz1').hide();
            $('.wiz2').hide();
            $('.wiz3').show();
        });       

    });

</script>

<script type="text/javascript" src="{{asset('js/jquery.qrcode.js')}}"></script>
<script type="text/javascript" src="{{asset('js/qrcode.js')}}"></script>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    @foreach($cointype as $cointypes)
        $('#qrcode-{{$cointypes->symbol}}').qrcode($('#qrcode-{{$cointypes->symbol}}').attr('ins'));
    @endforeach
  });
</script> -->
@endsection
@section('styles')
<style type="text/css">
    .pay-method label:hover .cmg-soon, .pay-method input:checked+label .cmg-soon{
        color: #fff;
    }


.promo-txt.no-promo {
    color: red !important;
}
.timeline{
  --uiTimelineMainColor: var(--timelineMainColor, #222);
  --uiTimelineSecondaryColor: var(--timelineSecondaryColor, #fff);

  position: relative;
  padding-top: 0;
  padding-bottom: 0;
}

.timeline:before{
  content: "";
  width: 4px;
  height: 100%;
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  top: 0;
}

.timeline__group{
  position: relative;
}

.timeline__group:not(:first-of-type){
  margin-top: 4rem;
}

.timeline__year{
  padding: .5rem 1.5rem;
  color: var(--uiTimelineSecondaryColor);
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  left: 0;
  top: 0;
  font-weight: 700;
}

@media screen and (min-width: 641px){
.timeline__date {
    top: 50%;
    margin-top: -27px;
    border-radius: 50px;
    min-width: auto;
    left: 12px;
}
}

.timeline__date {
    top: 50%;
    margin-top: -27px;
    border-radius: 50px !important;
    min-width: auto !important;
    left: 12px !important;
}

.timeline__box{
  position: relative;
}

.timeline__box:not(:last-of-type){
  margin-bottom: 30px;
}

.timeline__box:before{
  content: "";
  width: 100%;
  height: 2px;
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  left: 0;
  z-index: -1;
}

.timeline__date{
  min-width: 65px;
  position: absolute;
  left: 0;

  box-sizing: border-box;
  padding: .5rem 1.5rem;
  text-align: center;

  background-color: var(--uiTimelineMainColor);
  color: var(--uiTimelineSecondaryColor);
}

.timeline__day{
  font-size: 2rem;
  font-weight: 700;
  display: block;
}

.timeline__month{
  display: block;
  font-size: .8em;
  text-transform: uppercase;
}

.timeline__post{
  padding: 1.5rem 2rem;
  border-radius: 2px;
  border-left: 3px solid var(--uiTimelineMainColor);
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12), 0 1px 2px 0 rgba(0, 0, 0, .24);
  background-color: var(--uiTimelineSecondaryColor);
}

@media screen and (min-width: 641px){

  .timeline:before{
    left: 30px;
}

.timeline__group{
    padding-top: 55px;
}

.timeline__box{
    padding-left: 80px;
}

.timeline__box:before{
    top: 50%;
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);  
}  

.timeline__date{
    top: 50%;
    margin-top: -27px;
}
}

@media screen and (max-width: 640px){

  .timeline:before{
    left: 0;
}

.timeline__group{
    padding-top: 40px;
}

.timeline__box{
    padding-left: 20px;
    padding-top: 70px;
}

.timeline__box:before{
    top: 90px;
}    

.timeline__date{
    top: 0;
}
}

.timeline{
  --timelineMainColor: #195260;
  font-size: 16px;
}

@media screen and (min-width: 768px){

  html{
    font-size: 62.5%;
}
}

@media screen and (max-width: 767px){

  html{
    font-size: 55%;
}
}

/*
* demo page
*/

@media screen and (min-width: 768px){

  html{
    font-size: 62.5%;
}
}

@media screen and (max-width: 767px){

  html{
    font-size: 50%;
}
}


p{
  margin-top: 0;
  margin-bottom: 1.5rem;
  line-height: 1.5;
}

p:last-child{
  margin-bottom: 0;
}

.page{
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}



.main-container{
  max-width: 960px;
  padding-left: 2rem;
  padding-right: 2rem;

  margin-left: auto;
  margin-right: auto;
}

.page__container{
  padding-top: 30px;
  padding-bottom: 30px;
  max-width: 800px;
}

.footer{
  padding-top: 1rem;
  padding-bottom: 1rem;
  text-align: center;  
  font-size: 1.4rem;
}

.footer__link{
  text-decoration: none;
  color: inherit;
}

@media screen and (min-width: 361px){

  .footer__container{
    display: flex;
    justify-content: space-between;
}
}

@media screen and (max-width: 360px){

  .melnik909{
    display: none;
} 
}
</style>
@endsection