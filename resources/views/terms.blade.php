@extends('layouts.app')

@section('content')

    <div class="referral-section">
        <div class="container">
                    <div class="row">
                                    <section>
                                        <div class="authority">
                                            <h3>Terms And Services</h3>
                                            <h5 class="privacy-title">1. Introduction</h5>
                                            <p class="privacy-text">
                                                Please read the Terms and Service completely for dembycoin.io which is owned and operated by BelCrypto Global Exchanges HK Ltd. This agreement documents the legally binding terms and conditions that are attached to the site. By using this Website, you agree to accept all terms and conditions written here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions.</p>
                                        </div>
                                    </section>
                                    <section>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">2. Intellectual Property Rights</h5>
                                            <p class="privacy-text">Other than the content you own, under these Terms, Beldex and/or its licensors own all the intellectual property rights and materials contained in the Website.</p>
                                            <p>You are granted a limited license only for purposes of viewing the material contained on this Website.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">3. Restrictions</h5>
                                            <p class="privacy-text">You are specifically restricted from all of the following</p>
                                            <ul>
                                                <li>publishing any Website material in any other media;</li>
                                                <li>selling, sublicensing and/or otherwise commercializing any Website material;</li>
                                                <li>publicly performing and/or showing any Website material;</li>
                                                <li>using this Website in any way that is or may be damaging to this Website;</li>
                                                <li>using this Website in any way that impacts user access to this Website;</li>
                                                <li>using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li>
                                                <li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;</li>
                                                <li>Using this Website to engage in any advertising or marketing.
                                                </li>
                                            </ul>
                                            <p>Certain areas of this Website are restricted from being access by you and Beldex may further restrict access by you to any areas of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality as well.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">4. Your Content</h5>
                                            <p class="privacy-text">In these Website Standard Terms and Conditions, �Your Content� shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant Beldex a non-exclusive, worldwide irrevocable, sub-licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.</p>
                                            <p>Your Content must be original and must not be invading any third-party rights. Beldex reserves the right to remove any of Your Content from this Website at any time without notice.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">5. No warranties</h5>
                                            <p>This Website is provided �as is,� with all faults, and Beldex express no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">6. Limitation of liability</h5>
                                            <p class="privacy-text">In no event shall Beldex, nor any of its officers, directors, and employees shall be held liable for anything arising out of or in any way connected with your use of this Website whether such liability is under contract. Beldex, including its officers, directors, and employees shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>
                                            <p>We use cookies to:</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">7. Indemnification</h5>
                                            <p class="privacy-text">You hereby indemnify to the fullest extent Beldex from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.
                                            </p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">8. Severability</h5>
                                            <p class="privacy-text">If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">9. Variation of Terms</h5>
                                            <p class="privacy-text">Beldex is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these Terms on a regular basis.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">10. Assignment</h5>
                                            <p class="privacy-text">The Beldex is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">11. Entire Agreement</h5>
                                            <p class="privacy-text">These Terms constitute the entire agreement between Beldex and individual, in relation to the individual use of this Website and supersede all prior agreements and understandings.</p>
                                        </div>
                                        <div class="authority-text">
                                            <h5 class="privacy-title">12. Governing Law & Jurisdiction</h5>
                                            <p class="privacy-text">These Terms will be governed by and interpreted in accordance with the laws of the Country of Malaysia, and you submit to the non-exclusive jurisdiction of the state and federal courts located in Malaysia for the resolution of any disputes.</p>
                                        </div>
                                    </section>
                                </div>
        </div>
    </div>
@endsection

@section('styles')
<style type="text/css">
@media (max-width: 991px) {
    #myTable thead {
      display: none;
    }
    #myTable td {
      word-break: none;
    }
    #myTable td:nth-of-type(1):before { content: "S.No" ; }
    #myTable td:nth-of-type(2):before { content: "Name"; }
    #myTable td:nth-of-type(3):before { content: "Date"; }

    #myTable td:first-child.dataTables_empty {
      text-align:  center;
      width:  100%;
    }

    #myTable td:first-child.dataTables_empty:before {
      display:  none;
    }

    #myTable td::before {
      width: 25%;
      display: inline-block;
    }
    #myTable td {
      padding: 10px !important;
      width: 100%;
      display: inline-block;
      text-align: left;
    }
    .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border: 1px solid #cacaca;
    }
    #myTable td:last-child {
      border-bottom: 0 !important;
    }
    #myTable tbody tr {
      margin: 20px 0;
      display: inline-block;
      width: 100%;
      border: 1px solid #cacaca;
  }
  .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border-bottom: 1px solid #cecece !important;
  }
}
</style>
@endsection