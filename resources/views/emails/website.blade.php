<b>{{ Setting::get('site_title', 'Ico Investors')  }} | @lang('user.email.lead_contact')</b><br><br>
<b>@lang('user.profile.name') :</b>{{ $name }}<br><br>
<b>@lang('user.profile.email')  :</b>{{ $email }}<br><br>
<b>@lang('user.profile.phone')  :</b>{{ $phone }}<br><br>
<b>@lang('user.profile.message')  :</b>{{ $msg }}<br><br>
