<?php

return array (

	'common' => array (
	    'home' => 'Home',
	    'login' => 'Login',
	    'register' => 'Register',
	    'purchase' => 'Purchase',
		'deposit' => 'Deposit',
		'profile' => 'Profile',
	    'logout' => 'Logout',
	    'all_rights_reserved' => 'All rights reserved',
	    'terms_of_sales_agreement' => 'Terms of Sales Agreement',
	    'privacy_policy' => 'Privacy Policy',
	    'buy' => 'Buy',
	    'wallet' => 'Wallet',
	    'payment' => 'Payment',
	    'transaction' => 'Transactions',
	    'payments' => 'Payments',
	    'passbook' => 'Passbook',
	    'kyc' => 'KYC',
	    'referral' => 'Referral',
	    'kyc_documents' => 'KYC DOCUMENTS',
	    'upload_kyc_documents' => 'UPLOAD KYC DOCUMENTS',
	    'transaction_id' => 'Transaction id',
	    'submit' => 'Submit',
	    'thank_you' => 'Thank you',
	    'next' => 'Next',
	    'finish' => 'Finish',
	    'first' => 'First',
	    'previous' => 'Previous',
	    'upload' => 'Upload',
	    'bonus' => 'Bonus',
		'update_ether_address' => 'Please Update Your Contract Address in My ETHER',
		'eth_address' => 'ETH Address',
		'eth_wallet' => 'ETH Wallet',
		'btc_wallet' => 'BTC Wallet',
		'xrp_wallet' => 'XRP Wallet',
		'update' => 'UPDATE',
		'id' => 'ID',
		'txn' => 'TXN',
		'payment_method' => 'Payment Method',
		'ico_quantity'=> ':ico Quantity',
		'ico_price'=> ':ico Price',
		'price' => 'Price',
		'status' => 'Status',
		'date_time' => 'Date/Time',
		'no_record_found' => 'No record found',
		'received_through' => 'Received Through',
		'change_password'=>'Change Password',
		'save' => 'Save',
		'success' => 'Your transaction is confirmed please proceed ',
		'field_required' => 'This field is required',
		'bounty' => 'Bounty',
		'bounty_management' => 'Bounty Management',
		'referral_bounty' => 'Referral Bounty',
		'media_bounty' => 'Media Bounty',
		'erc_addresscontent' => 'To receive',
		'erc_addresscontent1' => 'you will need an ERC20-compliant ETH wallet. If you donot have one, create your ETH wallet for free at',
		'erc_addresscontent2' => 'Save Address',
	),
	'welcome' => array (
		'ico_investors'=> ':ico Investors',
	),
	'mail' => array (
		'mail_adminalert'=> 'submitted all Kyc document,Kindly verify it.',
		'generate_otp' => 'Generate OTP is :',
		'website' => 'Website',
		'admin_coin' => 'Token purchased from',
		'user_mail' => 'User Mail id :',
		'admin_transaction' => 'Transaction Invoice Number :',
		'final' => 'Final',
		'Queries' => 'For More Queries',
		'contact_us' => 'CONTACT US',
		'password_content' => 'Your password has been changed successfully. If you did not do this yourself, please click the below button to reset your password...',
		'click_reset' => 'Click & Reset',
		'differentip_content' => 'If you suspect that your account was used by someone else, please change your Password immediately and contact customer support.',
		'login_attempt' => 'Login attempt details',
		'ip_address' => 'IP address :',
		'country' => 'Country',
		'ip_content' => 'If you did not do this yourself ',
		'ip_content1' => 'Go to',
		'ip_content2' => ' and change your password, as somebody has tried to log in with your password',
		'login_start_content' => 'You have successfully logged into your',
		'account' => 'account',
		'login_start_content1' => 'Login attempt details ',
		'referral_content' => 'The participant ',
		'referral_content1' => 'that you had referred has successfully registered with',
		'referral_trans_content' => 'Your referral person',
		'referral_trans_content1' => 'has purchased',
		'referral_trans_content2' => 'Final USD',
		'referral_trans_content3' => 'Final price',
		'referral_trans_content4' => 'Percentage',
		'referral_trans_content5' =>'Referral Commission',
		'sign_upcontent' => 'Thanks for registering with',
		'sign_upcontent1'=>' Your registration was successful',
		'sign_upcontent2'=>'Click the following button to verify your email',
		'sign_upcontent3'=> 'Verify Email',
		'third_stepcontent'=> 'Congratulations you have successfully purchased',
		'third_stepcontent1' => 'coins',
		'lead_contact' => 'Lead Contacts',

	),
	'login' => array (
		'plan_token_purchase'=> 'Plan your :ico tokens purchase',
		'buy_token_by_cryto'=> 'You can buy :ico tokens using BTC, ETH, XRP cryptocurrencies',
		'sign_your_account'=> 'Sign into your account',
		'email_address'=> 'E-Mail Address',
		'password'=> 'Password',
		'password_confirmation'=> 'Password confirmation',
		'forgot_password'=> 'Forgot password',
		'click'=> 'Click',
		'here'=> 'here',
		'no_account'=> 'No account',
		'register'=> 'Register',
		'registration'=> 'Registration',
		'registration_confirmed'=> 'Registration Confirmed',
		'registered_success'=> 'You have successfully registered. An email is sent to you for verification',
		'sign_up'=> 'Sign Up',
		'sign_in'=> 'Sign In',
		'i_agree'=> 'I agree',
		'terms_of_services'=> 'Terms of Services',
		'have_an_account'=> 'Have an account',
		'no_part_material'=>'No part of this website or any of its contents may be reproduced, copied, modified or adapted, without the prior written consent of the author, unless otherwise indicated for stand-alone materials.',
		'reset_password'=> 'Reset Password',
		'confirm_password'=> 'Confirm Password',
		'send_password_reset_link'=> 'Send Password Reset Link',
		'email_verified_successfully'=> 'Your Email is successfully verified.  Click here to login',
		'forgot_password_login' => 'Forgot Your Password?',
		'dont_have_account' =>"Don't have an account?",
	),
	'home' => array (
	    'kyc_document_approval' => 'KYC DOCUMENT APPROVAL',
	    'sumbit_kyc_request' => 'Kindly submit your KYC document & Plan your :ico tokens purchase',
	    'welcome' => 'Welcome, :name',
	    'promocode_applied' => 'Promocode Applied',
	    'invalid_promocode' => 'Invalid Promocode',
	    'promocode_expired' => 'Promocode Expired',
	    'promocode_already_used' => 'Promocode already Used',
	    'promocode_applied' => 'Promocode Applied',
	    'invalid_transaction_id' => 'Invalid Transaction Id',
	    'price_not_match' => 'Price does not match',
	    'deposit_not_match' => 'Deposit Address does not match',
	    'transaction_processing_wait' => 'Please wait until your Transaction is Processing',
	    'payment_methods' => 'Payment Methods',
	    'payment_setup' => 'Payment Setup',
	    'transaction_detail' => 'Transaction Details',
	    'step' => 'Step',
	    'get' => 'Get',
	    'coins_using' => 'Coins using',
	    'continue' => 'continue',
	    'payment' => 'Payment',
	    'qty_buy' => 'quantity to buy',
	    'equivalent' => 'Equivalent',
	    'discount' => 'Discount',
	    'bck_options' => 'Back to payment options',
	    'nxt_stp' => 'next step',
	    'funds_only' => 'Funds Only!',
	    'send_only' => 'Send only',
	    'home_content' => 'to this address. The coins will be ',
	    'home_content1' => 'credited after we get confirmation from the network',
	    'exact' => 'Exact',
	    'home_content2' => 'amount will be calculated at time of transaction.',
	    'home_content3' => 'Awaiting Payment, Please donot close or refresh this window',
	    'payment_received' => 'Payment received',
	    'days' => 'Days',
	    'hours' => 'Hours',
	    'minutes'=> 'Minutes',
	    'seconds' => 'Seconds',
	    'to_address' => 'to this address',
	    'address' => 'Address',
	    'copy_clipboard' => 'copy to clipboard',
	    'home_content4' => 'Please note that the address is unique and it can used only for one transaction',
	    'proceed' => 'Proceed',
	    'home_content5' => 'Choose a payment option like BTC, ETH or XRP to buy your ',
	    'coin' => 'coin',
	    'home_content6' => 'Specify your value of',
	    'home_content7' => 'to buy through selected payment type',
	    'home_content8' => 'Scan the QR-code or copy the address given below to buy ',
	   'home_content9' => 'through selected cryptocurrency',
	   'home_content0' => 'Please note that the address of BTC (or) ETH is unique and it can used only for one transaction',
	   'confirmation' => 'Confirmation',
	   'modal_content' => 'Have you paid your payment, if not please use the address given and make your payment and click proceed again',
	   'modal_content1' =>'Once the payment have confirmed, your',
	   'modal_content2' => 'Coin will be credited to your wallet',
	   'modal_content3' => 'I have paid',
	   'close' => 'Close',
	),
	'kyc' => array(
		
		'kyc_content' => 'Purchases cannot be made until KYC documents are Verified', 
		'kyc_head' => 'KYC Details',
		'kyc_upload' =>'Upload Image',
		'kyc_content1' => 'Please make sure that the photo is complete and clearly visible, in JPG format.',
		'example' => 'Example',


	),
	    'buy' => array (
		    
		    'plan_purchase' => 'Plan your :ico tokens purchase',
		    'you_can_buy' => 'You can buy :ico tokens using BTC, ETH, XRP cryptocurrencies.
                    The calculator is provided for your convenience. You can enter a number of :ico tokens you want to buy and calculate the amount you would need to have in your account wallets.',
		    'select_coin_type' => 'Select Coin type',
		    'value' => 'Value',
		    'total' => 'TOTAL',
		    'bonus' => 'BONUS',
		    'enter_promo_code' => 'Enter Promo Code',
		    'apply_promo' => 'Apply Promo',
		    'make_a_deposit' => 'Make a deposit',
		    'cnt_strong_encryption' => 'We respect your privacy and protect it with strong encryption, plus strict policies',
		    'cnt_min_purchase_amount' => 'Please make sure your deposit equals or exceeds the minimum purchase amount',
		    'cnt_transaction_confirmations' => 'The funds will appear in your account wallets only after your cryptocurrency transaction gets 6 confirmations',
		    'deposit_scan_below' => 'Deposit funds by scanning below',
		    'or_direct_deposit' => 'Or Direct Deposit to',
		    'dest_tag' => 'Dest Tag',
		    'destination_tag' => 'Destination Tag',
		    'awaiting_payment' => 'Awaiting payment',
		    'buy_tokens' => 'Buy :ico tokens',	    
		    'less_than_order_amount' => 'Paid amount is less than the order amount. Please re-pay the exact amount',	    
		    
		),

	'wallet'=>array(
		'my_tokens'=>'My :ico Tokens',
		'my_bonus'=>'My :ico Bonus',
		'my_token_price'=>'My :ico Token Price',
		'for_purchase'=>'for purchase',
		'valid_till_to'=>'Valid Till :from To :to',
		'update_eth_address'=>'Update ETH Address',
	),
	'profile'=>array(
		'name'=>'Name',
		'email'=>'Email',
		'phone'=>'Phone',
		'message' => 'Message',
		'referal_link'=>'Referral Link',
		'copy_link'=>'Copy Link',
		'joined_date'=>'Joined Date',
		'select_country' => 'Select your country',
		'country' => 'Country',
		'accept_terms' => 'I accept',
		'terms_condition' => 'terms & conditions',
		'already_account' => 'Already have an account?',
		'to_receive' => 'To receive',
		'personal_info' => 'Personal information',
		'profile_content' => 'All your personal data provided here will be handled and maintained confidentially',
		'full_name' => 'Full name',
		'update_password' => 'Update Password',
		'profile_content1' => 'All your personal data provided here will be handled and maintained confidentially',
		'save_changes' => 'Save changes',
	),
	'password'=>array(
		'current_password'=>'Current Password',
		'enter_new_password'=>'Enter New Password',
		'confirm_password'=>'Confirm Password',

	),

	'referral' => array(
		'referral_content' => 'Join our referral program and share with your friends! Get',
		'referral_content1' => 'Coin from all your friend’s purchase!  Its a win-win for both',
		'referral_content2' => 'Share your unique link and refer friends',
		'gain' => 'Gain',
		'referral_content3' => 'coin your friends have bought',
		'referral_content4' => 'Share the invitation link to your friends and others.  Become a Great Referrer for every Sign-up by  your invitation!  Get the exciting',
		'referral_content5' => 'coins for every purchase by your friends',
		'share_fb' => 'Share via Facebook',
		'share_twt' => 'Share via Twitter',
		'share_email' => 'Share via Email',
		'referrals' => 'Referrals',
		's_no' => 'S.No',
		'name' => 'Name',
		'mail_id' => 'Mail ID',
		'date' => 'Date',
		'referral_content6' => 'You have no referrals. Invite your friends to get bonuses',
		'referral_content7' => 'Referral Users Transactions',
		'date_time' => 'Date/Time',
		'buy' => 'Buy',
		'no_record_found' => 'No record Found!',


	),

	'transaction' => array(

		'transaction_content' => 'Your',
		'referral' => 'Referral',
		'referral_link' => 'Get your Referral Link',
		'transactions' => 'Transactions',
		's_no' => 'Sl.No',
		'invoice' => 'Invoice',
		'date' => 'Date',
		'price' => 'Price',
		'value' => 'Value',
		'usd_value' => 'USD Value',
		'status' => 'Status',
		'archive' => 'Archive',
		'import' => 'Import',
		'transaction_content1' => 'Oops! The Page you requested was not found',
		'bck_home' => ' Back to Home',

	),



);