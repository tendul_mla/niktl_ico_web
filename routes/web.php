<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/siteapi/guzzlepostuser', 'AdminController@guzzlepostuser');

Route::get('/siteapi/guzzlesaveuser', 'AdminController@guzzlesaveuser');
Route::get('/siteapi/guzzleverifyuser', 'AdminController@guzzleverifyuser');

Route::get('/welcomemail', function () {
 
   return view('emails.changepasswordalert');
 
});

Route::get('/userimport','HomeController@userimport');
Route::post('/userimportstore','HomeController@userimportstore')->name('userimportstore');

Route::get('/emailtokengenerate', 'HomeController@emailtokengenerate');

Route::get('/webemail', 'HomeController@webemail');

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');


Route::get('/welcomemail', function () {
  
    return view('emails.changepasswordalert');
  
});

Route::group(['prefix' => 'coinadmin'], function () {
  Route::get('/login', 'CoinadminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'CoinadminAuth\LoginController@login');
  Route::post('/logout', 'CoinadminAuth\LoginController@logout')->name('logout');
  Route::get('/logout', 'CoinadminAuth\LoginController@logout');

  Route::get('/register', 'CoinadminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'CoinadminAuth\RegisterController@register');

  Route::post('/password/email', 'CoinadminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'CoinadminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CoinadminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CoinadminAuth\ResetPasswordController@showResetForm');
});


Route::get('/', function () {
  if(Auth::user()) {
    return redirect('home');
  } else {
    return view('auth.login');
  }
});


Auth::routes();


Route::group(['middleware' => ['preventbackbutton','auth']], function () {

  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/hometest', 'HomeController@indextest')->name('hometest');
  Route::post('/change/password', 'HomeController@update_password');
  Route::get('/profile', 'HomeController@profile');
  Route::post('/profile', 'HomeController@profile_store');
  Route::post('/address', 'HomeController@Address');
  Route::get('/kyc', 'HomeController@kyc');
  Route::post('/kyc', 'HomeController@kycProcess');
  Route::get('/referral', 'HomeController@referral');

//ercaddressupdate
  Route::post('/ercaddressupdate', 'HomeController@ercaddressupdate');
  
  Route::get('/coinstatus', 'HomeController@coinStatus');
  Route::get('/transaction', 'HomeController@getTransaction');
  Route::get('/checkTransaction', 'HomeController@checkTransaction');
  Route::post('/transaction', 'HomeController@Transaction');
  Route::get('/checkTrans', 'HomeController@checkTrans')->name('checktrans');
  Route::get('/checkbtctran', 'HomeController@history')->name('checkbtctran');
  Route::resource('passbook', 'PassbookController');

  Route::get('/paymentsecondstep', 'HomeController@paymentsecondstep');

  Route::get('{id}/achieve', 'HomeController@achieve')->name('achieve');

  Route::get('/offerpop', 'HomeController@offerpop')->name('offerpop');


  Route::get('/createbtcaddress', 'HomeController@createbtcaddress')->name('createbtcaddress');
  Route::get('/createethaddress', 'HomeController@createethaddress')->name('createethaddress');
  

});
Route::get('/cron', 'HomeController@cron');

Route::get('/terms', function () {
  
    return view('terms');
  
});
Route::get('/kyc2', function () {
  
    return view('kyc2');
  
});
Route::get('/whoops', function () {
  
    return view('whoops');
  
});


// Route::post('/change/password', 'HomeController@update_password');
// Route::get('/profile', 'HomeController@profile')->name('profile');
// Route::post('/profile', 'HomeController@profile_store');
// Route::get('/coinstatus', 'HomeController@coinCurrentStatus')->name('coinstatus');
// Route::get('/checkTransaction', 'HomeController@checkTransaction')->name('checktransation');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


